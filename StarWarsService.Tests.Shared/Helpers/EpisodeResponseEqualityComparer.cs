﻿using System.Collections.Generic;
using System.Linq;
using StarWarsService.Application.Dtos.Responses;

namespace StarWarsService.Tests.Shared.Helpers
{
    public class EpisodeResponseEqualityComparer : IEqualityComparer<EpisodeResponse>

    {
        public bool Equals(EpisodeResponse x, EpisodeResponse y)
        {
            if (ReferenceEquals(x, y)) return true;

            if (x is null || y is null) return false;

            if (x.Characters is null && y.Characters != null || x.Characters != null && y.Characters == null)
                return false;

            return x.Title == y.Title && x.Id == y.Id &&
                   (x.Characters == null && y.Characters == null || x.Characters.SequenceEqual(y.Characters));
        }

        public int GetHashCode(EpisodeResponse obj)
        {
            var titleHashCode = obj.Title.GetHashCode();
            return titleHashCode;
        }
    }
}