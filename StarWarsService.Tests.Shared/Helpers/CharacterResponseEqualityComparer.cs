﻿using System.Collections.Generic;
using System.Linq;
using StarWarsService.Application.Dtos.Responses;

namespace StarWarsService.Tests.Shared.Helpers
{
    public class CharacterResponseEqualityComparer : IEqualityComparer<CharacterResponse>
    {
        public bool Equals(CharacterResponse x, CharacterResponse y)
        {
            if (ReferenceEquals(x, y)) return true;

            if (x is null || y is null) return false;

            if (x.Friends is null && y.Friends != null || x.Friends != null && y.Friends == null) return false;

            if (x.Episodes is null && y.Episodes != null || x.Episodes != null && y.Episodes == null) return false;

            return x.Name == y.Name && (x.Episodes == null && y.Episodes == null ||
                                        x.Episodes.SequenceEqual(y.Episodes)) &&
                   (x.Friends == null && y.Friends == null || x.Friends.SequenceEqual(y.Friends)) && x.Id == y.Id;
        }

        public int GetHashCode(CharacterResponse obj)
        {
            var hashCodeName = obj.Name == null ? 0 : obj.Name.GetHashCode();
            var hashCodeFriends = obj.Friends == null ? 0 : obj.Friends.GetHashCode();
            var hashCodeEpisodes = obj.Episodes == null ? 0 : obj.Episodes.GetHashCode();

            return hashCodeName ^ hashCodeFriends ^ hashCodeEpisodes;
        }
    }
}