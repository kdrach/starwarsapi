﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using StarWarsService.Api.Infrastructure.Middleware.Swagger;
using StarWarsService.Domain.Interfaces;
using StarWarsService.Infrastructure.Repositories;

namespace StarWarsService.Api.Extensions
{
    public static class DependencyInjection
    {
        public static void ConfigureApiServices(this IServiceCollection services)
        {
            services.AddScoped<ICharactersRepository, CharactersRepository>();
            services.AddScoped<IEpisodeRepository, EpisodeRepository>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo {Title = "StarWars Api Service", Version = "v1", Description = "StarWars RestApi"});
                c.DocumentFilter<SwaggerDocumentFilter>();
                c.OperationFilter<SwaggerOperationFilter>();
                c.OperationFilter<SwaggerOperationSummaryFilter>();
                c.IncludeXmlComments(@"..\StarWarsService.Application\bin\Swagger.XML");
            });
        }
    }
}