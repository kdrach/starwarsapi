﻿namespace StarWarsService.Api.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsDefault<T>(this T value) where T : struct
        {
            var isDefault = value.Equals(default(T));

            return isDefault;
        }
    }
}