﻿using System.Collections.Generic;
using Microsoft.OpenApi.Models;
using StarWarsService.Api.Models;

namespace StarWarsService.Api.ValueTypes
{
    public class SwaggerMethodsDescriptions
    {
        private const string GetAllAsyncMethodName = "GetAllAsync";
        private const string GetByIdAsyncMethodName = "GetByIdAsync";
        private const string CreateAsyncMethodName = "CreateAsync";
        private const string DeleteAsyncMethodName = "DeleteAsync";
        private const string UpdateAsyncMethodName = "UpdateAsync";


        public static Dictionary<string, SwaggerOperation> CharacterControllerMethodDefinitions =
            new Dictionary<string, SwaggerOperation>
            {
                {
                    GetAllAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Get all characters asynchronously",
                        Description =
                            "The method returns the list of objects. The requested page is defined by page parameter, page size by the page size parameter. Page size limit is 50",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "GetAllAsync method ended successfully"}},
                            {"400", new OpenApiResponse {Description = "Page size or page number is null or empty"}},
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                },
                {
                    GetByIdAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Get requested character asynchronously",
                        Description =
                            "The method returns character object using Id from route.",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "GetByIdAsync method ended successfully"}},
                            {"404", new OpenApiResponse {Description = "Character not found"}},
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                },
                {
                    CreateAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Create character asynchronously",
                        Description =
                            "The method saves  a new object to the database using data from Request.",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "Character created successfully"}},
                            {
                                "400",
                                new OpenApiResponse
                                {
                                    Description =
                                        "Request is null or friend or episode from request not exists in database."
                                }
                            },
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                },
                {
                    DeleteAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Delete character asynchronously",
                        Description =
                            "The method deletes character with all relation from database.",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "Character deleted successfully"}},
                            {"404", new OpenApiResponse {Description = "Character not found"}},
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                },
                {
                    UpdateAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Update character asynchronously",
                        Description =
                            "The method updates character with all his relation in database.",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "Character deleted successfully"}},
                            {
                                "400",
                                new OpenApiResponse
                                {
                                    Description =
                                        "Request is null or friend or episode from request not exists in database."
                                }
                            },
                            {"404", new OpenApiResponse {Description = "Character not found"}},
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                }
            };

        public static Dictionary<string, SwaggerOperation> EpisodeControllerMethodDefinitions =
            new Dictionary<string, SwaggerOperation>
            {
                {
                    GetAllAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Get all episodes asynchronously",
                        Description =
                            "The method returns the list of objects. The requested page is defined by page parameter, page size by the page size parameter. Page size limit is 50",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "GetAllAsync method ended successfully"}},
                            {"400", new OpenApiResponse {Description = "Page size or page number is null or empty"}},
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                },
                {
                    GetByIdAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Get requested episode asynchronously",
                        Description =
                            "The method returns episode object using Id from route.",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "GetByIdAsync method ended successfully"}},
                            {"404", new OpenApiResponse {Description = "Episode not found"}},
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                },
                {
                    CreateAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Create episode asynchronously",
                        Description =
                            "The method saves a new episode to the database using data from Request.",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "Episode created successfully"}},
                            {
                                "400",
                                new OpenApiResponse
                                {
                                    Description =
                                        "Request is null."
                                }
                            },
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                },
                {
                    DeleteAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Delete episode asynchronously",
                        Description =
                            "The method deletes episode from database.",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "Episode deleted successfully"}},
                            {"404", new OpenApiResponse {Description = "Episode not found"}},
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                },
                {
                    UpdateAsyncMethodName,
                    new SwaggerOperation
                    {
                        Summary = "Update episode asynchronously",
                        Description =
                            "The method updates episode in database.",
                        Responses = new OpenApiResponses
                        {
                            {"200", new OpenApiResponse {Description = "Character updated successfully"}},
                            {
                                "400",
                                new OpenApiResponse
                                {
                                    Description =
                                        "Request is null or id is null."
                                }
                            },
                            {"404", new OpenApiResponse {Description = "Episode not found"}},
                            {"500", new OpenApiResponse {Description = "Unhandled exception occured"}}
                        }
                    }
                }
            };
    }
}