﻿using Microsoft.AspNetCore.Mvc;

namespace StarWarsService.Api.Controllers
{
    public class GetAllAsyncParameters
    {
        [FromQuery] public int Page { get; set; }

        [FromQuery] public int PageSize { get; set; }
    }
}