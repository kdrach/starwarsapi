﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace StarWarsService.Api.Controllers
{
    public class GetByIdParameters
    {
        [FromRoute] public Guid Id { get; set; }
    }
}