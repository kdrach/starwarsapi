﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace StarWarsService.Api.Controllers
{
    public class DeleteByIdParameters
    {
        [FromRoute] public Guid Id { get; set; }
    }
}