﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StarWarsService.Api.Infrastructure.Filters;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Interfaces;
using StarWarsService.Domain.Models;
using StarWarsService.Domain.ValueTypes;

namespace StarWarsService.Api.Controllers
{
    public class BaseController<T1, T2> : Controller where T1 : BaseRequest where T2 : BaseResponse
    {
        private readonly IGenericService<T1, T2> _genericService;

        public BaseController(IServiceFactory<T1, T2> serviceFactory)
        {
            _genericService = serviceFactory.Create(ControllerType);
        }

        protected virtual ControllerEnum ControllerType { get; }

        [HttpGet]
        [ValidateModel]
        public async Task<IActionResult> GetAllAsync(CancellationToken cancellationToken,
            GetAllAsyncParameters parameters)
        {
            var characters = await _genericService.GetAllAsync(cancellationToken,
                new PaginationOptions {PageNumber = parameters.Page, PageSize = parameters.PageSize});

            var metadata = new
            {
                characters.TotalCount,
                characters.PageSize,
                characters.CurrentPage,
                characters.TotalPages,
                characters.HasNext,
                characters.HasPrevious
            };

            Response?.Headers?.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

            return Ok(characters);
        }

        [HttpGet("{Id}")]
        [ValidateModel]
        public async Task<IActionResult> GetByIdAsync(GetByIdParameters parameters,
            CancellationToken cancellationToken)
        {
            if (parameters.Id == Guid.Empty) return NotFound();

            var character = await _genericService.GetAsync(parameters.Id, cancellationToken);
            if (character == null) return NotFound();

            return Ok(character);
        }


        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> CreateAsync([FromBody] T1 request,
            CancellationToken cancellationToken)
        {
            if (request == null) return BadRequest();
            T2 result;

            try
            {
                result = await _genericService.CreateAsync(request, cancellationToken);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(result);
        }


        [HttpPut]
        [ValidateModel]
        public async Task<IActionResult> UpdateAsync([FromBody] T1 request,
            CancellationToken cancellationToken)
        {
            if (request == null) return BadRequest();
            if (request.Id == Guid.Empty) return BadRequest("Id is required when updating object");
            T2 result;
            try
            {
                result = await _genericService.UpdateAsync(request, cancellationToken);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(result);
        }

        [HttpDelete("{Id}")]
        [ValidateModel]
        public async Task<IActionResult> DeleteAsync(DeleteByIdParameters parameters,
            CancellationToken cancellationToken)
        {
            if (parameters.Id == Guid.Empty) return NotFound();
            T2 result;
            try
            {
                result = await _genericService.DeleteAsync(parameters.Id, cancellationToken);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(result);
        }
    }
}