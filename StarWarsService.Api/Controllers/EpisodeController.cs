﻿using Microsoft.AspNetCore.Mvc;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Interfaces;
using StarWarsService.Domain.ValueTypes;

namespace StarWarsService.Api.Controllers
{
    [Route("api/[controller]")]
    public class EpisodeController : BaseController<EpisodeRequest, EpisodeResponse>
    {
        public EpisodeController(IServiceFactory<EpisodeRequest, EpisodeResponse> serviceFactory) : base(serviceFactory)
        {
        }

        protected override ControllerEnum ControllerType => ControllerEnum.EpisodeController;
    }
}