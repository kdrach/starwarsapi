﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StarWarsService.Api.Models;

namespace StarWarsService.Api.Infrastructure.Middleware
{
    public sealed class SerializedExceptionMiddleware
    {
        public const string DefaultErrorMessage = "Error occurred.";

        private readonly IWebHostEnvironment _env;
        private readonly JsonSerializer _serializer;

        public SerializedExceptionMiddleware(IWebHostEnvironment env)
        {
            _env = env;
            _serializer = new JsonSerializer
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }

        public async Task Invoke(HttpContext context)
        {
            context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
            context.Response.ContentType = "application/json";

            var ex = context.Features.Get<IExceptionHandlerFeature>()?.Error;
            if (ex == null) return;

            var error = BuildError(ex, _env);

            var writer = new StreamWriter(context.Response.Body);
            _serializer.Serialize(writer, error);
            await writer.FlushAsync().ConfigureAwait(false);
        }

        private static ApiError BuildError(Exception ex, IHostEnvironment env)
        {
            if (env.IsProduction())
                return BuildErrorForProdEnvironment(ex);
            return BuildErrorForNonProdEnvironment(ex);
        }

        private static ApiError BuildErrorForNonProdEnvironment(Exception ex)
        {
            return new ApiError {Details = ex.StackTrace, Message = ex.Message};
        }

        private static ApiError BuildErrorForProdEnvironment(Exception ex)
        {
            return new ApiError {Details = ex.Message, Message = DefaultErrorMessage};
        }
    }
}