﻿using System;
using System.Collections.Generic;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace StarWarsService.Api.Infrastructure.Middleware.Swagger
{
    public class SwaggerDocumentFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            if (swaggerDoc == null) throw new ArgumentNullException(nameof(swaggerDoc));

            swaggerDoc.Tags = new List<OpenApiTag>
            {
                new OpenApiTag {Name = "Character", Description = "Endpoint provides operation on character entities"},
                new OpenApiTag {Name = "Episode", Description = "Endpoint provides operation on episode entities"}
            };
        }
    }
}