﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace StarWarsService.Api.Infrastructure.Middleware.Swagger
{
    public class SwaggerOperationFilter : IOperationFilter
    {
        public const string PageParameterName = "Page";
        public const string PageSizeParameterName = "PageSize";


        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (IsMethodWithHttpGetAttribute(context))
            {
                var pageParameterIndex = operation.Parameters.ToList().FindLastIndex(i => i.Name == PageParameterName);
                if (pageParameterIndex == -1)
                    return;

                operation.Parameters[pageParameterIndex] = new OpenApiParameter
                {
                    Name = "Page",
                    In = ParameterLocation.Query,
                    Description = "Requested page number. Default value is 1.",
                    Required = false,
                    Example = new OpenApiInteger(1)
                };
                var pageSizeIndex = operation.Parameters.ToList().FindLastIndex(i => i.Name == PageSizeParameterName);
                if (pageSizeIndex == -1)
                    return;

                operation.Parameters[pageSizeIndex] = new OpenApiParameter
                {
                    Name = "PageSize",
                    In = ParameterLocation.Query,
                    Description = "Requested page size. Default value is 10.",
                    Required = false,
                    Example = new OpenApiInteger(10)
                };
            }

            ValidateInput(operation, context);
        }

        private void ValidateInput(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation == null) throw new ArgumentNullException(nameof(operation));

            if (context == null) throw new ArgumentNullException(nameof(context));

            if (operation.Parameters == null) operation.Parameters = new List<OpenApiParameter>();
        }

        private bool IsMethodWithHttpGetAttribute(OperationFilterContext context)
        {
            return context.MethodInfo.CustomAttributes.Any(attribute =>
                attribute.AttributeType == typeof(HttpGetAttribute));
        }
    }
}