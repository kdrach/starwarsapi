﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.OpenApi.Models;
using StarWarsService.Api.Controllers;
using StarWarsService.Api.Extensions;
using StarWarsService.Api.Models;
using StarWarsService.Api.ValueTypes;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace StarWarsService.Api.Infrastructure.Middleware.Swagger
{
    public class SwaggerOperationSummaryFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            switch (context.MethodInfo.ReflectedType?.Name)
            {
                case nameof(CharacterController):
                {
                    var swaggerOperationInformation =
                        SwaggerMethodsDescriptions.CharacterControllerMethodDefinitions.FirstOrDefault(m =>
                            m.Key == context.MethodInfo.Name);
                    AddMethodsProperties(swaggerOperationInformation, operation);
                    break;
                }
                case nameof(EpisodeController):
                {
                    var swaggerOperationInformation =
                        SwaggerMethodsDescriptions.EpisodeControllerMethodDefinitions.FirstOrDefault(m =>
                            m.Key == context.MethodInfo.Name);
                    AddMethodsProperties(swaggerOperationInformation, operation);
                    break;
                }
            }
        }

        private static void AddMethodsProperties(KeyValuePair<string, SwaggerOperation> swaggerOperationInformation,
            OpenApiOperation operation)
        {
            if (swaggerOperationInformation.IsDefault()) return;
            operation.Summary = swaggerOperationInformation.Value.Summary;
            operation.Description = swaggerOperationInformation.Value.Description;
            operation.Responses = swaggerOperationInformation.Value.Responses;
        }
    }
}