using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StarWarsService.Api.Extensions;
using StarWarsService.Api.Infrastructure.Middleware;
using StarWarsService.Application.Extensions;
using StarWarsService.Infrastructure.Extensions;

namespace StarWarsService.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureInfrastructureServices(Configuration);
            services.ConfigureApplicationServices();
            services.ConfigureApiServices();
            services.AddControllers();
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var exceptionMiddleware = new SerializedExceptionMiddleware(
                app.ApplicationServices.GetRequiredService<IWebHostEnvironment>());

            app.UseExceptionHandler(new ExceptionHandlerOptions {ExceptionHandler = exceptionMiddleware.Invoke});

            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(
                c => c.SwaggerEndpoint("v1/swagger.json", "StarWars Api Service"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}