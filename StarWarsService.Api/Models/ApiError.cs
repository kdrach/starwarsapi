﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace StarWarsService.Api.Models
{
    public sealed class ApiError
    {
        private const string ApiErrorMessage = "Invalid request";

        public ApiError()
        {
        }

        public ApiError(string message)
        {
            Message = message;
        }

        public ApiError(ModelStateDictionary modelState)
        {
            Message = ApiErrorMessage;
            var allErrors = modelState.Values.SelectMany(v => v.Errors.Select(b => b.ErrorMessage));
            Details = JsonConvert.SerializeObject(allErrors);
        }

        public string Message { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Details { get; set; }
    }
}