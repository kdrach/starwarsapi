﻿using Microsoft.OpenApi.Models;

namespace StarWarsService.Api.Models
{
    public class SwaggerOperation
    {
        public string Summary { get; set; }
        public string Description { get; set; }
        public OpenApiResponses Responses { get; set; }
    }
}