﻿using System.Collections.Generic;

namespace StarWarsService.Domain.Models
{
    public class Episode : BaseEntity
    {
        public string Title { get; set; }
        public virtual ICollection<CharacterEpisode> CharacterEpisodes { get; set; }
    }
}