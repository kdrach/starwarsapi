﻿using System.Collections.Generic;

namespace StarWarsService.Domain.Models
{
    public class Character : BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<CharacterEpisode> CharacterEpisodes { get; set; }
        public virtual ICollection<CharacterFriend> CharacterFriends { get; set; }
        public virtual ICollection<CharacterFriend> FriendOf { get; set; }
    }
}