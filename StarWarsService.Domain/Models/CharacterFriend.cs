﻿using System;

namespace StarWarsService.Domain.Models
{
    public class CharacterFriend
    {
        public Guid CharacterId { get; set; }
        public Character Character { get; set; }
        public Guid FriendId { get; set; }
        public Character Friend { get; set; }
    }
}