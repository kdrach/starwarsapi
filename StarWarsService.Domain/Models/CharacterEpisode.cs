﻿using System;

namespace StarWarsService.Domain.Models
{
    public class CharacterEpisode
    {
        public Character Character { get; set; }
        public Guid CharacterId { get; set; }
        public Episode Episode { get; set; }
        public Guid EpisodeId { get; set; }
    }
}