﻿namespace StarWarsService.Domain.Models
{
    public class PaginationOptions
    {
        private const int MaxPageSize = 50;
        private int _pageNumber = 1;

        private int _pageSize = 10;

        public int PageNumber
        {
            get => _pageNumber;
            set => _pageNumber = value > 0 ? value : _pageNumber;
        }

        public int PageSize
        {
            get => _pageSize;
            set
            {
                if (value > 0)
                    _pageSize = value > MaxPageSize ? MaxPageSize : value;
            }
        }
    }
}