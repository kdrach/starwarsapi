﻿using System;
using System.Collections.Generic;

namespace StarWarsService.Domain.Models
{
    public class PagedList<T> : List<T>
    {
        public PagedList(List<T> items = null, int count = 0, int pageNumber = 0, int pageSize = 0)
        {
            TotalCount = count;
            PageSize = pageSize;
            CurrentPage = pageNumber;
            TotalPages = (int) Math.Ceiling(count / (double) pageSize);

            if (items != null)
                AddRange(items);
        }

        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public bool HasPrevious => CurrentPage > 1;
        public bool HasNext => CurrentPage < TotalPages;
    }
}