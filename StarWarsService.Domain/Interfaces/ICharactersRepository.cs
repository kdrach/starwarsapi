﻿using StarWarsService.Domain.Models;

namespace StarWarsService.Domain.Interfaces
{
    public interface ICharactersRepository : IGenericRepository<Character>
    {
    }
}