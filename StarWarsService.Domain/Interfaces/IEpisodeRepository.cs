﻿using StarWarsService.Domain.Models;

namespace StarWarsService.Domain.Interfaces
{
    public interface IEpisodeRepository : IGenericRepository<Episode>
    {
    }
}