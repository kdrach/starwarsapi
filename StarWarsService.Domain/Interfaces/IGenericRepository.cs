﻿using System;
using System.Threading.Tasks;
using StarWarsService.Domain.Models;

namespace StarWarsService.Domain.Interfaces
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task<T> GetById(Guid id);
        Task Add(T entity);
        Task Update(T entity);
        Task Remove(T entity);
        Task<PagedList<T>> GetAll(PaginationOptions parameters);
    }
}