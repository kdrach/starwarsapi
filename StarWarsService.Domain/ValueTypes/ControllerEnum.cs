﻿namespace StarWarsService.Domain.ValueTypes
{
    public enum ControllerEnum
    {
        CharacterController = 1,
        EpisodeController = 2
    }
}