﻿using System.Linq;
using AutoMapper;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Domain.Models;

namespace StarWarsService.Application.Mapper
{
    /// <summary>
    ///     Class containing  default AutoMapper Profile used in entire solution
    /// </summary>
    public class DefaultAutoMappingProfile : Profile
    {
        /// <summary>
        ///     Default AutoMapper Profile
        /// </summary>
        public DefaultAutoMappingProfile()
        {
            CreateMap<CharacterResponse, Character>()
                .ForAllMembers(option => option.AllowNull());

            CreateMap<Character, CharacterResponse>()
                .ForMember(m => m.Episodes, o => o.MapFrom(s => s.CharacterEpisodes.Select(t => t.Episode.Title)))
                .ForMember(m => m.Friends, o => o.MapFrom(s => s.CharacterFriends.Select(t => t.Friend.Name).ToList()))
                .ForAllMembers(option => option.AllowNull());

            CreateMap<CharacterRequest, Character>()
                .ForMember(m => m.CharacterFriends, option => option.Ignore())
                .ForMember(m => m.CharacterEpisodes, o => o.Ignore())
                .ForAllMembers(option => option.AllowNull());

            CreateMap<Episode, EpisodeResponse>()
                .ForMember(c => c.Characters,
                    o => o.MapFrom(s => s.CharacterEpisodes.Select(c => c.Character.Name).ToArray()))
                .ReverseMap()
                .ForAllMembers(option => option.AllowNull());

            CreateMap<EpisodeRequest, Episode>()
                .ReverseMap()
                .ForAllMembers(option => option.AllowNull());


            CreateMap<PagedList<Character>, PagedList<CharacterResponse>>()
                .ForCtorParam("items", c => c.MapFrom(l => l.ToList()))
                .ForAllMembers(o => o.AllowNull());


            CreateMap<PagedList<Episode>, PagedList<EpisodeResponse>>()
                .ForCtorParam("items", c => c.MapFrom(l => l.ToList()))
                .ForAllMembers(o => o.AllowNull());
        }
    }
}