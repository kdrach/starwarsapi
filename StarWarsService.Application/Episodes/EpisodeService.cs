﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Domain.Interfaces;
using StarWarsService.Domain.Models;

namespace StarWarsService.Application.Episodes
{
    /// <inheritdoc />
    public class EpisodeService : IEpisodeService
    {
        private readonly IMapper _mapper;
        private readonly IEpisodeRepository _repository;

        /// <inheritdoc />
        public EpisodeService(IEpisodeRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<EpisodeResponse> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            var movie = await _repository.GetById(id);

            return _mapper.Map<Episode, EpisodeResponse>(movie);
        }

        /// <inheritdoc />
        public async Task<PagedList<EpisodeResponse>> GetAllAsync(CancellationToken cancellationToken,
            PaginationOptions parameters)
        {
            var movies = await _repository.GetAll(parameters);
            return _mapper.Map<PagedList<Episode>, PagedList<EpisodeResponse>>(movies);
        }

        /// <inheritdoc />
        public async Task<EpisodeResponse> DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var entityToDelete = await _repository.GetById(id);
            if (entityToDelete == null)
                throw new InvalidOperationException($"Episode with id {id} not exists");

            await _repository.Remove(entityToDelete);
            return _mapper.Map<Episode, EpisodeResponse>(entityToDelete);
        }

        /// <inheritdoc />
        public async Task<EpisodeResponse> CreateAsync(EpisodeRequest request, CancellationToken cancellationToken)
        {
            var entityToSave = _mapper.Map<EpisodeRequest, Episode>(request);
            await _repository.Add(entityToSave);
            return _mapper.Map<Episode, EpisodeResponse>(entityToSave);
        }

        /// <inheritdoc />
        public async Task<EpisodeResponse> UpdateAsync(EpisodeRequest request, CancellationToken cancellationToken)
        {
            var entityToSave = _mapper.Map<EpisodeRequest, Episode>(request);
            await _repository.Update(entityToSave);
            return _mapper.Map<Episode, EpisodeResponse>(entityToSave);
        }
    }
}