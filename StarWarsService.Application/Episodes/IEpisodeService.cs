﻿using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Interfaces;

namespace StarWarsService.Application.Episodes
{
    /// <summary>
    ///     Interface used for operation on episode entity
    /// </summary>
    public interface IEpisodeService : IGenericService<EpisodeRequest, EpisodeResponse>
    {
    }
}