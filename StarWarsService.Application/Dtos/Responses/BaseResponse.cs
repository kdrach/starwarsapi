﻿using System;

namespace StarWarsService.Application.Dtos.Responses
{
    /// <summary>
    ///     Class representing a base response
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        ///     Object Id
        /// </summary>
        public Guid Id { get; set; }
    }
}