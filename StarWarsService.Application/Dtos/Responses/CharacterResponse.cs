﻿namespace StarWarsService.Application.Dtos.Responses
{
    /// <summary>
    ///     Character's Response from Create,Delete or update method
    /// </summary>
    public class CharacterResponse : BaseResponse
    {
        /// <summary>
        ///     Character's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Array which contains titles of episodes with character
        /// </summary>
        public string[] Episodes { get; set; }

        /// <summary>
        ///     Array which contains names of character's friends
        /// </summary>
        public string[] Friends { get; set; }
    }
}