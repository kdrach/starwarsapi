﻿namespace StarWarsService.Application.Dtos.Responses
{
    /// <summary>
    ///     Episode Response from Create, Update or Delete methods
    /// </summary>
    public class EpisodeResponse : BaseResponse
    {
        /// <summary>
        ///     Episode Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Array which contains episode's characters names
        /// </summary>
        public string[] Characters { get; set; }
    }
}