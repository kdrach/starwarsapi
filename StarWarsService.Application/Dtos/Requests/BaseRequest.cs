﻿using System;

namespace StarWarsService.Application.Dtos.Requests
{
    /// <summary>
    ///     Class representing a base request
    /// </summary>
    public class BaseRequest
    {
        /// <summary>
        ///     Object Id
        /// </summary>
        public Guid Id { get; set; }
    }
}