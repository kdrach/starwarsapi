﻿using System.ComponentModel.DataAnnotations;

namespace StarWarsService.Application.Dtos.Requests
{
    /// <summary>
    ///     Episode request to create or update episode
    /// </summary>
    public class EpisodeRequest : BaseRequest
    {
        /// <summary>
        ///     Episode title
        /// </summary>
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
    }
}