﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StarWarsService.Application.Dtos.Requests
{
    /// <summary>
    ///     Request to update or create character
    /// </summary>
    public class CharacterRequest : BaseRequest
    {
        /// <summary>
        ///     Character Name
        /// </summary>
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        /// <summary>
        ///     Ids of Character's Episodes
        /// </summary>
        [Required(ErrorMessage = "Episodes are required")]
        public Guid[] Episodes { get; set; }

        /// <summary>
        ///     Ids of Character's Friends
        /// </summary>
        public Guid[] Friends { get; set; }
    }
}