﻿using System.ComponentModel;
using StarWarsService.Application.Characters;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Episodes;
using StarWarsService.Application.Interfaces;
using StarWarsService.Domain.ValueTypes;

namespace StarWarsService.Application
{
    /// <inheritdoc />
    public class ServicesFactory<T1, T2> : IServiceFactory<T1, T2> where T1 : BaseRequest where T2 : BaseResponse
    {
        private readonly ICharacterService _characterService;
        private readonly IEpisodeService _episodeService;

        /// <inheritdoc />
        public ServicesFactory(ICharacterService characterService, IEpisodeService episodeService)
        {
            _characterService = characterService;
            _episodeService = episodeService;
        }

        /// <inheritdoc />
        public IGenericService<T1, T2> Create(ControllerEnum controllerEnum)
        {
            switch (controllerEnum)
            {
                case ControllerEnum.CharacterController:
                    return (IGenericService<T1, T2>) _characterService;
                case ControllerEnum.EpisodeController:
                    return (IGenericService<T1, T2>) _episodeService;
                default:
                    throw new InvalidEnumArgumentException("Value is not valid");
            }
        }
    }
}