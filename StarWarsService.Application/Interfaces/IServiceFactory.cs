﻿using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Domain.ValueTypes;

namespace StarWarsService.Application.Interfaces
{
    /// <summary>
    ///     Service factory used in base controller
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public interface IServiceFactory<in T1, T2> where T1 : BaseRequest where T2 : BaseResponse
    {
        /// <summary>
        ///     Creates service based on controllerEnum value
        /// </summary>
        /// <param name="controllerEnum">Possible values CharacterController=1 , EpisodeController = 2</param>
        /// <returns></returns>
        IGenericService<T1, T2> Create(ControllerEnum controllerEnum);
    }
}