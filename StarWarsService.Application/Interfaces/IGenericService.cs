﻿using System;
using System.Threading;
using System.Threading.Tasks;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Domain.Models;

namespace StarWarsService.Application.Interfaces
{
    /// <summary>
    ///     Interface for Generic Service
    /// </summary>
    /// <typeparam name="T1">BaseRequest used in solution</typeparam>
    /// <typeparam name="T2">BaseResponse used in solution</typeparam>
    public interface IGenericService<in T1, T2> where T1 : BaseRequest where T2 : BaseResponse
    {
        /// <summary>
        ///     Get object asynchronously using id
        /// </summary>
        /// <param name="id">Object id (Guid)</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<T2> GetAsync(Guid id, CancellationToken cancellationToken);

        /// <summary>
        ///     Get all objects asynchronously
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="parameters">Parameters contains page size and page number </param>
        /// <returns>Object of type BaseResponse</returns>
        Task<PagedList<T2>> GetAllAsync(CancellationToken cancellationToken, PaginationOptions parameters);

        /// <summary>
        ///     Delete object asynchronously using id
        /// </summary>
        /// <param name="id">Object id (Guid)</param>
        /// <param name="cancellationToken"></param>
        /// <returns>List of object with pagination</returns>
        Task<T2> DeleteAsync(Guid id, CancellationToken cancellationToken);

        /// <summary>
        ///     Creates object asynchronously
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>Deleted object of type BaseResponse</returns>
        Task<T2> CreateAsync(T1 request, CancellationToken cancellationToken);

        /// <summary>
        ///     Updates object asynchronously
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>Updated object of type BaseResponse</returns>
        Task<T2> UpdateAsync(T1 request, CancellationToken cancellationToken);
    }
}