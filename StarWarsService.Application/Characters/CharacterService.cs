﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Domain.Interfaces;
using StarWarsService.Domain.Models;

#pragma warning disable 1591

namespace StarWarsService.Application.Characters
{
    public class CharacterService : ICharacterService
    {
        private readonly ICharactersRepository _charactersRepository;
        private readonly IEpisodeRepository _episodesRepository;
        private readonly IMapper _mapper;

        public CharacterService(ICharactersRepository charactersRepository, IMapper mapper,
            IEpisodeRepository episodesRepository)
        {
            _charactersRepository = charactersRepository;
            _episodesRepository = episodesRepository;
            _mapper = mapper;
        }

        public async Task<CharacterResponse> CreateAsync(CharacterRequest request, CancellationToken cancellationToken)
        {
            var characterEntity = _mapper.Map<CharacterRequest, Character>(request);
            await AssignFriends(request, characterEntity);
            await AssignMovies(request, characterEntity);

            await _charactersRepository.Add(characterEntity);
            return _mapper.Map<Character, CharacterResponse>(characterEntity);
        }

        public async Task<CharacterResponse> DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var entityToDelete = await _charactersRepository.GetById(id);

            if (entityToDelete == null)
                throw new InvalidOperationException($"Character with id : {id} does not exist in database");

            await RemoveRelationWithFriends(entityToDelete);
            await _charactersRepository.Remove(entityToDelete);
            return _mapper.Map<Character, CharacterResponse>(entityToDelete);
        }

        public async Task<CharacterResponse> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            var character = await _charactersRepository.GetById(id);
            return _mapper.Map<Character, CharacterResponse>(character);
        }

        public async Task<PagedList<CharacterResponse>> GetAllAsync(CancellationToken cancellationToken,
            PaginationOptions parameters)
        {
            var characters = await _charactersRepository.GetAll(parameters);
            return _mapper.Map<PagedList<Character>, PagedList<CharacterResponse>>(characters);
        }


        public async Task<CharacterResponse> UpdateAsync(CharacterRequest request, CancellationToken cancellationToken)
        {
            var character = await _charactersRepository.GetById(request.Id);

            if (character == null)
                throw new InvalidOperationException($"Character with id : {request.Id} does not exist in database");

            await UpdateCharacterRelationWithFriends(request, character);
            await AssignMovies(request, character);
            character.Name = request.Name;
            await _charactersRepository.Update(character);
            return _mapper.Map<Character, CharacterResponse>(character);
        }

        private async Task UpdateCharacterRelationWithFriends(CharacterRequest request, Character character)
        {
            var friendFromRequest = request.Friends == null
                ? Enumerable.Empty<Character>().ToList()
                : await GetAllFriendsFromCharacterRequest(request);

            var friendsToDelete = character.CharacterFriends == null
                ? Enumerable.Empty<CharacterFriend>()
                : character.CharacterFriends.Where(c =>
                    !friendFromRequest.Select(ce => ce.Id).ToList().Contains(c.CharacterId));

            foreach (var friendToDelete in friendsToDelete) await RemoveSingleFriendRelation(friendToDelete);
            AssignFriendRelationToEntities(character, friendFromRequest);
        }


        private async Task RemoveRelationWithFriends(Character entityToDelete)
        {
            foreach (var friendRelation in entityToDelete?.CharacterFriends)
                await RemoveSingleFriendRelation(friendRelation);
            var emptyRelationList = Enumerable.Empty<CharacterFriend>().ToList();

            entityToDelete.CharacterFriends = emptyRelationList;
            entityToDelete.FriendOf = emptyRelationList;
        }

        private async Task RemoveSingleFriendRelation(CharacterFriend friendRelation)
        {
            var friend = await _charactersRepository.GetById(friendRelation.FriendId);
            var relationWith = friend.CharacterFriends?.FirstOrDefault(e => e.CharacterId == friendRelation.FriendId);
            friend.CharacterFriends?.Remove(relationWith);
            var friendOfRelation = friend.FriendOf?.FirstOrDefault(e => e.FriendId == friendRelation.FriendId);
            friend.FriendOf?.Remove(friendOfRelation);
        }

        private async Task AssignMovies(CharacterRequest request, Character character)
        {
            if (request.Episodes == null)
                return;

            var moviesEntities = await GetAllMoviesFromCharacterRequest(request);

            character.CharacterEpisodes = moviesEntities.Select(s => new CharacterEpisode
            {
                Character = character, CharacterId = character.Id, Episode = s, EpisodeId = s.Id
            }).ToList();
        }

        private async Task<List<Episode>> GetAllMoviesFromCharacterRequest(CharacterRequest request)
        {
            var movies = request.Episodes;
            var moviesEntities = new List<Episode>();
            foreach (var id in movies)
            {
                var movie = await _episodesRepository.GetById(id);
                if (movie == null)
                    throw new InvalidOperationException($"Movie with id : {id} does not exist in database");
                moviesEntities.Add(movie);
            }

            return moviesEntities;
        }

        private async Task AssignFriends(CharacterRequest request, Character character)
        {
            if (request.Friends == null)
            {
                character.CharacterFriends = Enumerable.Empty<CharacterFriend>().ToList();
                return;
            }

            var friendsEntities = await GetAllFriendsFromCharacterRequest(request);

            AssignFriendRelationToEntities(character, friendsEntities);
        }

        private static void AssignFriendRelationToEntities(Character character, List<Character> friendsEntities)
        {
            character.CharacterFriends = friendsEntities.Select(s => new CharacterFriend
            {
                Character = character, CharacterId = character.Id, FriendId = s.Id, Friend = s
            }).ToList();

            character.FriendOf = friendsEntities.Select(f => new CharacterFriend
                {Character = f, CharacterId = f.Id, Friend = character, FriendId = character.Id}).ToList();
        }

        private async Task<List<Character>> GetAllFriendsFromCharacterRequest(CharacterRequest request)
        {
            var friendsEntities = new List<Character>();
            var friends = request.Friends;
            foreach (var id in friends)
            {
                var friend = await _charactersRepository.GetById(id);
                if (friend == null)
                    throw new InvalidOperationException($"Character with id : {id} does not exist in database");
                friendsEntities.Add(friend);
            }

            return friendsEntities;
        }
    }
}