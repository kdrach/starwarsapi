﻿using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Interfaces;

namespace StarWarsService.Application.Characters
{
    /// <summary>
    ///     Interface used for operation on character entity
    /// </summary>
    public interface ICharacterService : IGenericService<CharacterRequest, CharacterResponse>
    {
    }
}