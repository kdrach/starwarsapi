﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using StarWarsService.Application.Characters;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Episodes;
using StarWarsService.Application.Interfaces;
using StarWarsService.Application.Mapper;

namespace StarWarsService.Application.Extensions
{
    /// <summary>
    ///     Class containing the extension method for configuring services used in the Application project
    /// </summary>
    public static class DependencyInjection
    {
        /// <summary>
        ///     Method registering the Application Project Services
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureApplicationServices(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(DefaultAutoMappingProfile));

            services.AddScoped<ICharacterService, CharacterService>();
            services.AddScoped<IEpisodeService, EpisodeService>();
            services
                .AddScoped<IServiceFactory<EpisodeRequest, EpisodeResponse>,
                    ServicesFactory<EpisodeRequest, EpisodeResponse>>();
            services
                .AddScoped<IServiceFactory<CharacterRequest, CharacterResponse>,
                    ServicesFactory<CharacterRequest, CharacterResponse>>();
        }
    }
}