﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StarWarsService.Infrastructure;

namespace StarWarsService.IntegrationTests.Extensions
{
    public static class DependencyInjection
    {
        public static void ConfigureTestDatabaseConnection(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<StarWarsContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("TestConnection")));
        }
    }
}