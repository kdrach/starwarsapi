﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection;
using StarWarsService.Application.Dtos.Responses;

namespace StarWarsService.IntegrationTests.Extensions
{
    public static class TestExtensions
    {
        public static void SortArrayPropertiesOfType<T1, T2>(this T1 someObject)
        {
            var arrayProperties = GetProperties(someObject).Where(p => p.PropertyType == typeof(T2[])).ToArray();

            foreach (var arrayProperty in arrayProperties)
            {
                var propertyValue = arrayProperty.GetValue(someObject, null);
                if (propertyValue == null)
                    return;
                Array.Sort((T2[]) propertyValue);
            }
        }

        public static T[] SortCollectionUsingSqlGuid<T>(this IEnumerable<T> baseResponses) where T : BaseResponse
        {
            return baseResponses.OrderBy(c => (SqlGuid) c.Id).ToArray();
        }

        public static void SortPropertiesInAllObjectsFromResponsesArray<T>(this T[] deserializedResponse)
        {
            foreach (var response in deserializedResponse)
                response.SortArrayPropertiesOfType<T, string>();
        }

        private static PropertyInfo[] GetProperties(object obj)
        {
            return obj.GetType().GetProperties();
        }
    }
}