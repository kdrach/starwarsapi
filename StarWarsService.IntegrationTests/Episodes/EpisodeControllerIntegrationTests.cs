﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NUnit.Framework;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.IntegrationTests.Client;
using StarWarsService.IntegrationTests.Extensions;
using StarWarsService.IntegrationTests.ValueTypes;
using StarWarsService.Tests.Shared.Helpers;

namespace StarWarsService.IntegrationTests.Episodes
{
    [TestFixture]
    public class EpisodeControllerIntegrationTests : BaseControllerIntegrationTests<EpisodeResponse, EpisodeRequest>
    {
        private string CharacterEndpointAddress => "api/Character";
        protected override string ApiEndpointAddress => "api/Episode";
        protected override EpisodeResponse[] TestResponses => EpisodesTestsObjects.EpisodeResponses;
        protected override IEqualityComparer<EpisodeResponse> EqualityComparer => new EpisodeResponseEqualityComparer();
        protected override Guid ExistingObjectGuid => EpisodesTestsObjects.ExistingEpisodeGuid;
        public override Guid NotExistingObjectGuid => EpisodesTestsObjects.NotExistingEpisodeGuid;
        private CharacterCaller CharacterCaller => new CharacterCaller(Client, CharacterEndpointAddress);

        protected override HttpCallerBase<EpisodeResponse, EpisodeRequest> HttpCaller =>
            new EpisodeCaller(Client, ApiEndpointAddress);

        [TestCase]
        public async Task DeleteAsync_Should_Return_Deleted_Object()
        {
            //Arrange
            var expectedResponse = TestResponses.FirstOrDefault(s => s.Id == ExistingObjectGuid).Copy();
            expectedResponse.SortArrayPropertiesOfType<EpisodeResponse, string>();

            //Act
            var response = await HttpCaller.GetDeserializedResponseFromDelete(ExistingObjectGuid);

            //Clenup 
            RecreateDatabase();

            //Assert
            Assert.True(EqualityComparer.Equals(expectedResponse, response));
        }

        [TestCase]
        public async Task DeleteAsync_Should_Remove_All_Object_Relation()
        {
            //Arrange
            var expectedResponse = TestResponses.FirstOrDefault(s => s.Id == ExistingObjectGuid).Copy();
            expectedResponse.SortArrayPropertiesOfType<EpisodeResponse, string>();

            //Act
            await HttpCaller.GetDeserializedResponseFromDelete(ExistingObjectGuid);

            //Assert
            var deserializedCharacterResponse = await CharacterCaller.GetDeserializedResponseFromGetAllAsync();

            Assert.True(
                deserializedCharacterResponse.Select(d => d.Episodes.Contains(expectedResponse.Title)).ToList()
                    .All(i => i != true), "All characters relation should be deleted");

            //Cleanup 
            RecreateDatabase();
        }

        [TestCase]
        public async Task CreateAsync_Should_Return_Bad_Request_When_Title_Is_Missing()
        {
            //Arrange
            var episodeRequest = new EpisodeRequest();
            var jsonString = JsonConvert.SerializeObject(episodeRequest);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PostAsync(ApiEndpointAddress, content);
            var responseAsString = await response.Content.ReadAsStringAsync();

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
            Assert.AreEqual(responseAsString,
                "{\"message\":\"Invalid request\",\"details\":\"[\\\"Title is required\\\"]\"}");
        }

        [TestCase]
        public async Task CreateAsync_Should_Return_Object_When_Everything_Is_Correct()
        {
            //Arrange
            var title = "DON'T LEGALIZE IT";

            var episodeRequest = new EpisodeRequest
            {
                Title = title
            };
            var expectedResponse = new EpisodeResponse
            {
                Title = title,
                Characters = Enumerable.Empty<string>().ToArray()
            };
            expectedResponse.SortArrayPropertiesOfType<EpisodeResponse, string>();

            //Act
            var deserializedResponse = await HttpCaller.GetDeserializedResponseFromCreateAsync(episodeRequest);
            //Assert
            expectedResponse.Id = deserializedResponse.Id;

            Assert.True(EqualityComparer.Equals(expectedResponse, deserializedResponse));

            var deserializedGetNewObjectResponse =
                await HttpCaller.GetDeserializedResponseFromGetById(deserializedResponse.Id);
            Assert.True(EqualityComparer.Equals(expectedResponse, deserializedGetNewObjectResponse));

            //Cleanup
            RecreateDatabase();
        }

        [TestCase]
        public async Task GetAllAsync_Should_Contains_Newly_Created_Object()
        {
            //Arrange
            var title = "DON'T LEGALIZE IT";
            var episodeRequest = new EpisodeRequest
            {
                Title = title
            };
            var expectedResponse = new EpisodeResponse
            {
                Title = title,
                Characters = Enumerable.Empty<string>().ToArray()
            };
            expectedResponse.SortArrayPropertiesOfType<EpisodeResponse, string>();

            //Act
            var deserializedResponse = await HttpCaller.GetDeserializedResponseFromCreateAsync(episodeRequest);
            //Assert
            expectedResponse.Id = deserializedResponse.Id;

            Assert.True(EqualityComparer.Equals(expectedResponse, deserializedResponse));

            var deserializedGetNewObjectResponse = await HttpCaller.GetDeserializedResponseFromGetAllAsync();
            var newlyCreatedObject =
                deserializedGetNewObjectResponse.FirstOrDefault(c => c.Id == deserializedResponse.Id);

            Assert.True(EqualityComparer.Equals(expectedResponse, newlyCreatedObject));

            //Cleanup
            RecreateDatabase();
        }

        [TestCase]
        public async Task UpdateAsync_Should_Return_Bad_Request_When_Name_Is_Missing()
        {
            //Arrange
            var episodeRequest = new EpisodeRequest();
            var jsonString = JsonConvert.SerializeObject(episodeRequest);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PutAsync(ApiEndpointAddress, content);
            var responseAsString = await response.Content.ReadAsStringAsync();

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
            Assert.AreEqual(responseAsString,
                "{\"message\":\"Invalid request\",\"details\":\"[\\\"Title is required\\\"]\"}");
        }


        [TestCase]
        public async Task UpdateAsync_Should_Return_Updated_Object_When_Everything_Is_Correct()
        {
            //Arrange
            var title = "New Title";
            var episodeId = TestResponses[0].Id;

            var episodeRequest = new EpisodeRequest
            {
                Title = title,
                Id = episodeId
            };
            var expectedResponse = new EpisodeResponse
            {
                Title = title,
                Id = episodeId,
                Characters = Enumerable.Empty<string>().ToArray()
            };
            //Act
            var response = await HttpCaller.GetDeserializedResponseFromUpdateAsync(episodeRequest);
            //Cleanup
            RecreateDatabase();
            //Assert
            Assert.True(EqualityComparer.Equals(expectedResponse, response));
        }

        [TestCase]
        public async Task GetByIdAsync_Should_Return_New_Values_After_Update()
        {
            //Arrange
            var title = "New Title";
            var episodeId = TestResponses[0].Id;
            var editedObject = await HttpCaller.GetDeserializedResponseFromGetById(episodeId);

            var episodeRequest = new EpisodeRequest
            {
                Id = episodeId,
                Title = title
            };
            var expectedResponse = new EpisodeResponse
            {
                Id = episodeId,
                Title = title,
                Characters = editedObject.Characters
            };
            expectedResponse.SortArrayPropertiesOfType<EpisodeResponse, string>();

            //Act
            await HttpCaller.GetDeserializedResponseFromUpdateAsync(episodeRequest);
            var deserializedResponse = await HttpCaller.GetDeserializedResponseFromGetById(episodeId);

            //Cleanup
            RecreateDatabase();

            //Assert
            Assert.True(EqualityComparer.Equals(expectedResponse, deserializedResponse));
        }


        [TestCase]
        public async Task GetAllAsync_Should_Contain_Updated_Object_After_Update()
        {
            //Arrange
            var title = "New Title";
            var episodeId = TestResponses[0].Id;
            var editedObject = await HttpCaller.GetDeserializedResponseFromGetById(episodeId);

            var episodeRequest = new EpisodeRequest
            {
                Title = title,
                Id = episodeId
            };

            var expectedResponse = new EpisodeResponse
            {
                Title = title,
                Id = episodeId,
                Characters = editedObject.Characters
            };
            expectedResponse.SortArrayPropertiesOfType<EpisodeResponse, string>();

            //Act
            await HttpCaller.GetDeserializedResponseFromUpdateAsync(episodeRequest);

            var deserializedResponse = await HttpCaller.GetDeserializedResponseFromGetAllAsync();
            var updatedObject = deserializedResponse.FirstOrDefault(o => o.Id == episodeId);
            updatedObject.SortArrayPropertiesOfType<EpisodeResponse, string>();

            //Cleanup
            RecreateDatabase();

            //Assert
            Assert.True(EqualityComparer.Equals(expectedResponse, updatedObject));
        }
    }
}