﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NUnit.Framework;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Infrastructure;
using StarWarsService.IntegrationTests.Client;
using StarWarsService.IntegrationTests.Database;
using StarWarsService.IntegrationTests.Extensions;
using StarWarsService.IntegrationTests.Startup;

namespace StarWarsService.IntegrationTests
{
    public abstract class BaseControllerIntegrationTests<T1, T2> where T1 : BaseResponse where T2 : BaseRequest
    {
        private readonly int _defaultPageNumber = 1;


        private readonly int _defaultPageSize = 10;

        private StarWarsContext _dbContext;

        private WebApplicationFactory<TestStartup> _factory;
        protected HttpClient Client;

        protected abstract HttpCallerBase<T1, T2> HttpCaller { get; }
        protected abstract string ApiEndpointAddress { get; }
        protected abstract T1[] TestResponses { get; }
        protected abstract IEqualityComparer<T1> EqualityComparer { get; }
        protected abstract Guid ExistingObjectGuid { get; }
        public abstract Guid NotExistingObjectGuid { get; }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var projectDir = Directory.GetCurrentDirectory();
            var configurationPath = Path.Combine(projectDir, "testsettings.json");

            _factory = new StarWarsApiFactory<TestStartup>().WithWebHostBuilder(b =>
            {
                b.ConfigureAppConfiguration((context, conf) => { conf.AddJsonFile(configurationPath); });
            });
            _dbContext = (StarWarsContext) _factory.Services.GetService(typeof(StarWarsContext));
            RecreateDatabase();
            Client = _factory.CreateClient();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            _dbContext.Database.EnsureDeleted();
            _factory.Dispose();
        }

        [TestCase]
        public async Task GetAllAsync_Should_Return_Firsts_Objects_Without_Specifying_Pagination_Parameters()
        {
            //Arrange
            var recordsCount = TestResponses.Length;
            var totalPageCount = (recordsCount - 1) / _defaultPageSize + 1;

            var expectedPaginationHeader =
                "{\"TotalCount\"" +
                $":{recordsCount}," +
                "\"PageSize\"" +
                $":{_defaultPageSize}," +
                "\"CurrentPage\":1,\"" +
                "TotalPages\"" +
                $":{totalPageCount},\"" +
                "HasNext\":" +
                $"{(recordsCount > _defaultPageSize).ToString().ToLowerInvariant()},\"" +
                "HasPrevious\":false}";
            var expectedResponse = TestResponses.SortCollectionUsingSqlGuid().ToArray();
            expectedResponse.SortPropertiesInAllObjectsFromResponsesArray();

            //Act
            var response = await Client.GetAsync(ApiEndpointAddress);

            //Assert
            response.EnsureSuccessStatusCode();
            Assert.AreEqual(expectedPaginationHeader, response.Headers.GetValues("X-Pagination").First());
            var responseContent = await response.Content.ReadAsStringAsync();
            var deserializedResponse =
                (T1[]) JsonConvert.DeserializeObject(responseContent, typeof(T1[]));
            deserializedResponse.SortPropertiesInAllObjectsFromResponsesArray();
            deserializedResponse = deserializedResponse.SortCollectionUsingSqlGuid();
            Assert.True(expectedResponse.SequenceEqual(deserializedResponse, EqualityComparer));
        }


        [TestCase(1, 2)]
        [TestCase(5, 2)]
        [TestCase(2, 3)]
        [TestCase(1, 9)]
        [TestCase(1, 10)]
        public async Task GetAllAsync_Should_Return_Firsts_Objects_With_Specifying_PaginationParameters(int pageSize,
            int pageNumber)
        {
            //Arrange
            var recordsCount = TestResponses.Length;
            var totalPageCount = (recordsCount - 1) / pageSize + 1;

            var expectedPaginationHeader =
                "{\"TotalCount\"" +
                $":{recordsCount}," +
                "\"PageSize\"" +
                $":{pageSize}," +
                "\"CurrentPage\"" +
                $":{pageNumber},\"" +
                "TotalPages\"" +
                $":{totalPageCount},\"" +
                "HasNext\":" +
                $"{(totalPageCount > pageNumber).ToString().ToLowerInvariant()},\"" +
                "HasPrevious\"" +
                $":{(pageNumber > 1).ToString().ToLowerInvariant()}"
                + "}";
            var expectedResponse = TestResponses.SortCollectionUsingSqlGuid().Skip(pageSize * (pageNumber - 1))
                .Take(pageSize).ToArray();
            expectedResponse.SortPropertiesInAllObjectsFromResponsesArray();

            var queryString = ApiEndpointAddress + $"/?PageSize={pageSize}&Page={pageNumber}";

            //Act
            var response = await Client.GetAsync(queryString);

            //Assert
            response.EnsureSuccessStatusCode();
            Assert.AreEqual(expectedPaginationHeader, response.Headers.GetValues("X-Pagination").First());
            var responseContent = await response.Content.ReadAsStringAsync();
            var deserializedResponse =
                (T1[]) JsonConvert.DeserializeObject(responseContent, typeof(T1[]));
            deserializedResponse = deserializedResponse.SortCollectionUsingSqlGuid();
            deserializedResponse.SortPropertiesInAllObjectsFromResponsesArray();
            Assert.True(expectedResponse.SequenceEqual(deserializedResponse, EqualityComparer));
        }


        [TestCase]
        public async Task GetAllAsync_Should_Return_Firsts_Objects_When_PageSize_And_Page_Count_Are_Equal_Zero()
        {
            //Arrange
            var pageNumber = 0;
            var pageSize = 0;
            var recordsCount = TestResponses.Length;
            var totalPageCount = (recordsCount - 1) / _defaultPageSize + 1;

            var expectedPaginationHeader =
                "{\"TotalCount\"" +
                $":{recordsCount}," +
                "\"PageSize\"" +
                $":{_defaultPageSize}," +
                "\"CurrentPage\"" +
                $":{_defaultPageNumber},\"" +
                "TotalPages\"" +
                $":{totalPageCount},\"" +
                "HasNext\":" +
                $"{(totalPageCount > _defaultPageSize).ToString().ToLowerInvariant()},\"" +
                "HasPrevious\"" +
                $":{(pageNumber > 1).ToString().ToLowerInvariant()}"
                + "}";
            var expectedResponse = TestResponses.Skip(_defaultPageSize * (_defaultPageNumber - 1))
                .Take(_defaultPageSize).SortCollectionUsingSqlGuid().ToArray();
            expectedResponse.SortPropertiesInAllObjectsFromResponsesArray();

            var queryString = ApiEndpointAddress + $"/?PageSize={pageSize}&Page={pageNumber}";

            //Act
            var response = await Client.GetAsync(queryString);

            //Assert
            response.EnsureSuccessStatusCode();
            Assert.AreEqual(expectedPaginationHeader, response.Headers.GetValues("X-Pagination").First());
            var responseContent = await response.Content.ReadAsStringAsync();
            var deserializedResponse =
                (T1[]) JsonConvert.DeserializeObject(responseContent, typeof(T1[]));
            deserializedResponse.SortPropertiesInAllObjectsFromResponsesArray();
            deserializedResponse = deserializedResponse.SortCollectionUsingSqlGuid();
            Assert.True(expectedResponse.SequenceEqual(deserializedResponse, EqualityComparer));
        }


        [TestCase]
        public async Task GetAllAsync_Should_Returns_Bad_Request_When_Query_Is_Not_Correct()
        {
            //Arrange
            var queryString = ApiEndpointAddress + "/?PageSize=&PageNumber=";

            //Act
            var response = await Client.GetAsync(queryString);

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
        }

        [TestCase]
        public async Task GetById_Should_Return_Bad_Request_When_Id_Is_Empty()
        {
            //Arrange
            var emptyId = Guid.Empty;
            var queryString = ApiEndpointAddress + $"/{emptyId}";

            //Act
            var response = await Client.GetAsync(queryString);

            //Assert
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode, "Status code should be 404");
        }

        [TestCase]
        public async Task GetById_Should_Return_Not_Found_When_Object_Not_Exists_In_Database()
        {
            //Arrange
            var queryString = ApiEndpointAddress + $"/{NotExistingObjectGuid}";

            //Act
            var response = await Client.GetAsync(queryString);

            //Assert
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode, "Status code should be 404");
        }


        [TestCase]
        public async Task GetByIdAsync_Should_Return_Object_When_It_Exists_In_Database()
        {
            //Arrange
            var queryString = ApiEndpointAddress + $"/{ExistingObjectGuid}";
            var expectedResponse = TestResponses.FirstOrDefault(s => s.Id == ExistingObjectGuid);
            expectedResponse.SortArrayPropertiesOfType<T1, string>();
            //Act
            var deserializedResponse = await HttpCaller.GetDeserializedResponseFromGetById(ExistingObjectGuid);

            //Assert
            Assert.True(EqualityComparer.Equals(expectedResponse, deserializedResponse));
        }

        [TestCase]
        public async Task GetAll_Async_Response_Should_Not_Contain_Object_With_Was_Previously_Deleted()
        {
            //Arrage
            await HttpCaller.GetDeserializedResponseFromDelete(ExistingObjectGuid);

            //Act
            var deserializedResponse = await HttpCaller.GetDeserializedResponseFromGetAllAsync();

            //Assert
            Assert.Null(deserializedResponse.FirstOrDefault(c => c.Id == ExistingObjectGuid));

            //Cleanup
            RecreateDatabase();
        }


        [TestCase]
        public async Task DeleteAsync_Should_Return_Not_Found_When_It_Not_Exists_In_Database()
        {
            //Arrange
            var queryString = ApiEndpointAddress + $"/{NotExistingObjectGuid}";
            //Act
            var response = await Client.DeleteAsync(queryString);

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
        }


        [TestCase]
        public async Task GetByIdAsync_Should_Return_Not_Found_After_Object_Removal()
        {
            //Arrange
            var queryString = ApiEndpointAddress + $"/{ExistingObjectGuid}";
            //Act
            var response = await Client.DeleteAsync(queryString);
            response.EnsureSuccessStatusCode();

            var findResponse = await Client.GetAsync(queryString);
            //Assert
            Assert.AreEqual(HttpStatusCode.NotFound, findResponse.StatusCode);

            //Cleanup after test
            RecreateDatabase();
        }

        [TestCase]
        public async Task UpdateAsync_Should_Return_Bad_Request_When_Request_Not_Contains_Id()
        {
            //Arrange
            var queryString = ApiEndpointAddress;
            var request = new BaseRequest {Id = Guid.Empty};
            var jsonString = JsonConvert.SerializeObject(request);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            //Act
            var response = await Client.PutAsync(queryString, content);

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
        }


        [TestCase]
        public async Task UpdateAsync_Should_Return_Bad_Request_When_Object_Not_Exists_Id_Database()
        {
            //Arrange
            var queryString = ApiEndpointAddress;
            var request = new BaseRequest {Id = NotExistingObjectGuid};
            var jsonString = JsonConvert.SerializeObject(request);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            //Act
            var response = await Client.PutAsync(queryString, content);

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }


        [TestCase]
        public async Task UpdateAsync_Should_Return_Bad_Request_When_Request_Is_Null()
        {
            //Arrange
            var queryString = ApiEndpointAddress;
            var expectedResponse = TestResponses.FirstOrDefault(s => s.Id == ExistingObjectGuid);
            expectedResponse.SortArrayPropertiesOfType<T1, string>();
            var request = (BaseRequest) null;
            var jsonString = JsonConvert.SerializeObject(request);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PutAsync(queryString, content);

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }


        [TestCase]
        public async Task CreateAsync_Should_Return_Bad_Request_When_Request_Is_Null()
        {
            //Arrange
            var request = (BaseRequest) null;
            var jsonString = JsonConvert.SerializeObject(request);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PostAsync(ApiEndpointAddress, content);

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
        }


        protected void RecreateDatabase()
        {
            _dbContext.Database.EnsureDeleted();
            _dbContext.Database.EnsureCreated();
            DatabaseTestUtilities.Seed(_dbContext);
        }
    }
}