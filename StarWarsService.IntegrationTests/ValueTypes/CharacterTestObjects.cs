﻿using System;
using System.Linq;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Domain.Models;
using StarWarsService.IntegrationTests.Extensions;

namespace StarWarsService.IntegrationTests.ValueTypes
{
    public static class CharacterTestObjects
    {
        public static Guid NotExistingCharacterGuid = Guid.NewGuid();

        public static Domain.Models.Character HanSolo = new Domain.Models.Character
        {
            Name = "Han Solo",
            Id = Guid.NewGuid()
        };

        public static Domain.Models.Character LukeSkywalker = new Domain.Models.Character
        {
            Name = "Luke Skywalker",
            Id = Guid.NewGuid()
        };

        public static Domain.Models.Character LeiaOregana = new Domain.Models.Character
        {
            Name = "Leia Oregana",
            Id = Guid.NewGuid()
        };

        public static Domain.Models.Character Yoda = new Domain.Models.Character
        {
            Name = "Yoda",
            Id = Guid.NewGuid()
        };

        public static Domain.Models.Character ObiWanKenobi = new Domain.Models.Character
        {
            Name = "Obi-Wan Kenobi",
            Id = Guid.NewGuid()
        };

        public static Domain.Models.Character MaceWindu = new Domain.Models.Character
        {
            Name = "Mace Windu",
            Id = Guid.NewGuid()
        };

        public static Domain.Models.Character Palpatine = new Domain.Models.Character
        {
            Name = "Palpatine",
            Id = Guid.NewGuid()
        };

        public static Domain.Models.Character AnakinSkywalker = new Domain.Models.Character
        {
            Name = "Anakin Skywalker",
            Id = Guid.NewGuid()
        };

        public static Domain.Models.Character WilhuffTarkin = new Domain.Models.Character
        {
            Name = "Wilhuff Tarkin",
            Id = Guid.NewGuid()
        };

        public static Domain.Models.Character[] Characters =
        {
            HanSolo,
            LukeSkywalker,
            LeiaOregana,
            Yoda,
            ObiWanKenobi,
            MaceWindu,
            Palpatine,
            AnakinSkywalker,
            WilhuffTarkin
        };

        public static CharacterEpisode[] CharacterEpisodes =
            EpisodesTestsObjects.TestEpisodes.Select(e => new CharacterEpisode
                    {Character = HanSolo, CharacterId = HanSolo.Id, Episode = e, EpisodeId = e.Id})
                .Concat(
                    EpisodesTestsObjects.TestEpisodes.Select(e => new CharacterEpisode
                        {Character = LukeSkywalker, CharacterId = LukeSkywalker.Id, Episode = e, EpisodeId = e.Id})
                )
                .Concat(
                    EpisodesTestsObjects.TestEpisodes.Select(e => new CharacterEpisode
                        {Character = LeiaOregana, CharacterId = LeiaOregana.Id, Episode = e, EpisodeId = e.Id})
                )
                .Concat(EpisodesTestsObjects.TestEpisodes.SkipLast(3).Select(e => new CharacterEpisode
                    {Character = Yoda, CharacterId = Yoda.Id, Episode = e, EpisodeId = e.Id}))
                .Concat(EpisodesTestsObjects.TestEpisodes
                    .SkipLast(3).Select(e => new CharacterEpisode
                        {Character = ObiWanKenobi, CharacterId = ObiWanKenobi.Id, Episode = e, EpisodeId = e.Id}))
                .Concat(
                    EpisodesTestsObjects.TestEpisodes.SkipLast(8).Select(e => new CharacterEpisode
                        {Character = MaceWindu, CharacterId = MaceWindu.Id, Episode = e, EpisodeId = e.Id})
                )
                .Concat(EpisodesTestsObjects.TestEpisodes.Select(e => new CharacterEpisode
                    {Character = Palpatine, CharacterId = Palpatine.Id, Episode = e, EpisodeId = e.Id}))
                .Concat(EpisodesTestsObjects.TestEpisodes
                    .Select(e => new CharacterEpisode
                        {Character = AnakinSkywalker, CharacterId = AnakinSkywalker.Id, Episode = e, EpisodeId = e.Id})
                    .Concat(EpisodesTestsObjects.TestEpisodes.Skip(3).SkipLast(3).Select(e => new CharacterEpisode
                        {Character = WilhuffTarkin, CharacterId = WilhuffTarkin.Id, Episode = e, EpisodeId = e.Id}))
                ).ToArray();

        public static CharacterFriend[] CharacterFriends =
            Characters.Skip(1).SkipLast(3)
                .Select(c => new CharacterFriend
                    {Character = HanSolo, CharacterId = HanSolo.Id, Friend = c, FriendId = c.Id})
                .Concat(Characters.Where(c => c.Id != LukeSkywalker.Id).SkipLast(3).Select(c => new CharacterFriend
                    {Character = LukeSkywalker, CharacterId = LukeSkywalker.Id, Friend = c, FriendId = c.Id}))
                .Concat(Characters.Where(c => c.Id != LeiaOregana.Id).SkipLast(3).Select(c => new CharacterFriend
                    {Character = LeiaOregana, CharacterId = LeiaOregana.Id, Friend = c, FriendId = c.Id}))
                .Concat(Characters.Where(c => c.Id != Yoda.Id).SkipLast(3).Select(c => new CharacterFriend
                    {Character = Yoda, CharacterId = Yoda.Id, Friend = c, FriendId = c.Id}))
                .Concat(Characters.Where(c => c.Id != ObiWanKenobi.Id).SkipLast(3).Select(c => new CharacterFriend
                    {Character = ObiWanKenobi, CharacterId = ObiWanKenobi.Id, Friend = c, FriendId = c.Id}))
                .Concat(Characters.Where(c => c.Id != MaceWindu.Id).SkipLast(3).Select(c => new CharacterFriend
                    {Character = MaceWindu, CharacterId = MaceWindu.Id, Friend = c, FriendId = c.Id}))
                .Concat(Characters.Skip(6).Where(c => c.Id != Palpatine.Id).Select(c => new CharacterFriend
                    {Character = Palpatine, CharacterId = Palpatine.Id, Friend = c, FriendId = c.Id}))
                .Concat(Characters.Skip(6).Where(c => c.Id != AnakinSkywalker.Id).Select(c => new CharacterFriend
                    {Character = AnakinSkywalker, CharacterId = AnakinSkywalker.Id, Friend = c, FriendId = c.Id}))
                .Concat(Characters.Skip(6).Where(c => c.Id != WilhuffTarkin.Id).Select(c => new CharacterFriend
                    {Character = WilhuffTarkin, CharacterId = WilhuffTarkin.Id, Friend = c, FriendId = c.Id}))
                .ToArray();


        public static CharacterResponse[] CharacterResponses =
            Characters.Select(c => new CharacterResponse
            {
                Id = c.Id,
                Name = c.Name,
                Episodes = CharacterEpisodes.Where(ce => ce.CharacterId == c.Id).Select(n => n.Episode.Title).ToArray(),
                Friends = CharacterFriends.Where(cf => cf.CharacterId == c.Id).Select(fn => fn.Friend.Name).ToArray()
            }).SortCollectionUsingSqlGuid();
    }
}