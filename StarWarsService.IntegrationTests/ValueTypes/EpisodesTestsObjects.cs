﻿using System;
using System.Linq;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Domain.Models;

namespace StarWarsService.IntegrationTests.ValueTypes
{
    public static class EpisodesTestsObjects
    {
        public static Episode[] TestEpisodes =
        {
            new Episode {Id = Guid.NewGuid(), Title = "A New Hope"},
            new Episode {Id = Guid.NewGuid(), Title = "The Empire Strikes Back"},
            new Episode {Id = Guid.NewGuid(), Title = "Return of the Jedi"},
            new Episode {Id = Guid.NewGuid(), Title = "The Phantom Menace"},
            new Episode {Id = Guid.NewGuid(), Title = "Attack of the Clones"},
            new Episode {Id = Guid.NewGuid(), Title = "Revenge of the Sith"},
            new Episode {Id = Guid.NewGuid(), Title = "The Force Awakens"},
            new Episode {Id = Guid.NewGuid(), Title = "The Last Jedi"},
            new Episode {Id = Guid.NewGuid(), Title = "The Rise of Skywalker"}
        };

        public static Guid NotExistingEpisodeGuid = Guid.NewGuid();
        public static Guid ExistingEpisodeGuid = TestEpisodes[0].Id;

        public static EpisodeResponse[] EpisodeResponses =
            TestEpisodes.Select(e => new EpisodeResponse
            {
                Id = e.Id,
                Title = e.Title,
                Characters = CharacterTestObjects.CharacterEpisodes.Where(ce => ce.EpisodeId == e.Id)
                    .Select(t => t.Character.Name)
                    .ToArray()
            }).ToArray();
    }
}