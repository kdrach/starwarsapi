﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NUnit.Framework;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.IntegrationTests.Client;
using StarWarsService.IntegrationTests.Extensions;
using StarWarsService.IntegrationTests.ValueTypes;
using StarWarsService.Tests.Shared.Helpers;

namespace StarWarsService.IntegrationTests.Character
{
    [TestFixture]
    public class
        CharacterControllerIntegrationTests : BaseControllerIntegrationTests<CharacterResponse, CharacterRequest>
    {
        protected override string ApiEndpointAddress => "api/Character";
        private string EpisodeEndpointAddress => "api/Episode";

        protected override HttpCallerBase<CharacterResponse, CharacterRequest> HttpCaller =>
            new CharacterCaller(Client, ApiEndpointAddress);

        private EpisodeCaller EpisodeCaller => new EpisodeCaller(Client, EpisodeEndpointAddress);
        protected override Guid ExistingObjectGuid => CharacterTestObjects.MaceWindu.Id;
        public override Guid NotExistingObjectGuid => CharacterTestObjects.NotExistingCharacterGuid;
        protected override CharacterResponse[] TestResponses => CharacterTestObjects.CharacterResponses;

        protected override IEqualityComparer<CharacterResponse> EqualityComparer { get; } =
            new CharacterResponseEqualityComparer();


        [TestCase]
        public async Task DeleteAsync_Should_Return_Deleted_Object()
        {
            //Arrange
            var expectedResponse = TestResponses.FirstOrDefault(s => s.Id == ExistingObjectGuid).Copy();
            expectedResponse.SortArrayPropertiesOfType<CharacterResponse, string>();
            expectedResponse.Friends = Enumerable.Empty<string>().ToArray();

            //Act
            var response = await HttpCaller.GetDeserializedResponseFromDelete(ExistingObjectGuid);

            //Clenup 
            RecreateDatabase();

            //Assert
            Assert.True(EqualityComparer.Equals(expectedResponse, response));
        }


        [TestCase]
        public async Task DeleteAsync_Should_Remove_All_Object_Relation()
        {
            //Arrange
            var queryString = ApiEndpointAddress + $"/{ExistingObjectGuid}";
            var expectedResponse = TestResponses.FirstOrDefault(s => s.Id == ExistingObjectGuid).Copy();
            expectedResponse.SortArrayPropertiesOfType<CharacterResponse, string>();
            expectedResponse.Friends = Enumerable.Empty<string>().ToArray();

            //Act
            var response = await Client.DeleteAsync(queryString);
            response.EnsureSuccessStatusCode();

            //Assert
            var deserializedCharacterResponse = await HttpCaller.GetDeserializedResponseFromGetAllAsync();
            var getAllEpisodesResponse = await EpisodeCaller.GetDeserializedResponseFromGetAllAsync();

            Assert.True(
                deserializedCharacterResponse.Select(d => d.Friends.Contains(expectedResponse.Name)).ToList()
                    .All(i => i != true), "All Friends relation should be deleted");
            Assert.True(
                getAllEpisodesResponse.Select(d => d.Characters.Contains(expectedResponse.Name)).ToList()
                    .All(i => i != true), "All Episode relation should be deleted");

            //Clenup 
            RecreateDatabase();
        }

        [TestCase]
        public async Task CreateAsync_Should_Return_Bad_Request_When_Name_Is_Missing()
        {
            //Arrange
            var characterRequest = new CharacterRequest
                {Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Id).ToArray()};
            var jsonString = JsonConvert.SerializeObject(characterRequest);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PostAsync(ApiEndpointAddress, content);
            var responseAsString = await response.Content.ReadAsStringAsync();

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
            Assert.AreEqual(responseAsString,
                "{\"message\":\"Invalid request\",\"details\":\"[\\\"Name is required\\\"]\"}");
        }

        [TestCase]
        public async Task CreateAsync_Should_Return_Bad_Request_When_Episodes_Are_Missing()
        {
            //Arrange
            var characterRequest = new CharacterRequest
                {Name = "Cory"};
            var jsonString = JsonConvert.SerializeObject(characterRequest);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PostAsync(ApiEndpointAddress, content);
            var responseAsString = await response.Content.ReadAsStringAsync();

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
            Assert.AreEqual(responseAsString,
                "{\"message\":\"Invalid request\",\"details\":\"[\\\"Episodes are required\\\"]\"}");
        }

        [TestCase]
        public async Task CreateAsync_Should_Return_Bad_Request_When_Episodes_And_Name_Are_Missing()
        {
            //Arrange
            var characterRequest = new CharacterRequest();

            var jsonString = JsonConvert.SerializeObject(characterRequest);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PostAsync(ApiEndpointAddress, content);
            var responseAsString = await response.Content.ReadAsStringAsync();

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
            Assert.AreEqual(responseAsString,
                "{\"message\":\"Invalid request\",\"details\":\"[\\\"Name is required\\\",\\\"Episodes are required\\\"]\"}");
        }


        [TestCase]
        public async Task CreateAsync_Should_Return_Object_When_Everything_Is_Correct()
        {
            //Arrange
            var characterName = "Trevor";
            var characterFriends = CharacterTestObjects.Characters.Take(2).ToList();

            var characterRequest = new CharacterRequest
            {
                Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Id).ToArray(),
                Friends = characterFriends.Select(s => s.Id).ToArray(), Name = characterName
            };
            var expectedResponse = new CharacterResponse
            {
                Name = characterName, Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Title).ToArray(),
                Friends = characterFriends.Select(s => s.Name).ToArray()
            };
            expectedResponse.SortArrayPropertiesOfType<CharacterResponse, string>();

            //Act
            var deserializedResponse = await HttpCaller.GetDeserializedResponseFromCreateAsync(characterRequest);
            //Assert
            expectedResponse.Id = deserializedResponse.Id;

            Assert.True(EqualityComparer.Equals(expectedResponse, deserializedResponse));

            var deserializedGetNewObjectResponse =
                await HttpCaller.GetDeserializedResponseFromGetById(deserializedResponse.Id);
            Assert.True(EqualityComparer.Equals(expectedResponse, deserializedGetNewObjectResponse));

            //Cleanup
            RecreateDatabase();
        }


        [TestCase]
        public async Task CreateAsync_Should_Add_Friend_Relations()
        {
            //Arrange
            var characterName = "Lahey";
            var characterFriends = CharacterTestObjects.Characters.Take(2).ToList();

            var characterRequest = new CharacterRequest
            {
                Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Id).ToArray(),
                Friends = characterFriends.Select(s => s.Id).ToArray(), Name = characterName
            };
            var expectedResponse = new CharacterResponse
            {
                Name = characterName,
                Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Title).ToArray(),
                Friends = characterFriends.Select(s => s.Name).ToArray()
            };
            expectedResponse.SortArrayPropertiesOfType<CharacterResponse, string>();

            await HttpCaller.GetDeserializedResponseFromCreateAsync(characterRequest);

            //Act & Assert
            foreach (var friend in characterFriends)
            {
                var friendResponse = await Client.GetAsync(ApiEndpointAddress + $"/{friend.Id}");
                friendResponse.EnsureSuccessStatusCode();
                var friendResponseAsString = await friendResponse.Content.ReadAsStringAsync();
                var deserializedFriend =
                    (CharacterResponse) JsonConvert.DeserializeObject(friendResponseAsString,
                        typeof(CharacterResponse));
                Assert.True(deserializedFriend.Friends.Contains(characterName), "Friend relation should be added");
            }

            //Cleanup
            RecreateDatabase();
        }


        [TestCase]
        public async Task CreateAsync_Should_Add_Episode_Relations()
        {
            //Arrange
            var characterName = "Randy";
            var characterFriends = CharacterTestObjects.Characters.Take(2).ToList();
            var characterEpisodes = EpisodesTestsObjects.TestEpisodes;
            var characterRequest = new CharacterRequest
            {
                Episodes = characterEpisodes.Select(s => s.Id).ToArray(),
                Friends = characterFriends.Select(s => s.Id).ToArray(), Name = characterName
            };
            var expectedResponse = new CharacterResponse
            {
                Name = characterName,
                Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Title).ToArray(),
                Friends = characterFriends.Select(s => s.Name).ToArray()
            };
            expectedResponse.SortArrayPropertiesOfType<CharacterResponse, string>();

            await HttpCaller.GetDeserializedResponseFromCreateAsync(characterRequest);

            //Act & Assert
            foreach (var episode in characterEpisodes)
            {
                var episodeResponse = await EpisodeCaller.GetDeserializedResponseFromGetById(episode.Id);
                Assert.True(episodeResponse.Characters.Contains(characterName), "Episode relation should be added");
            }

            //Cleanup
            RecreateDatabase();
        }

        [TestCase]
        public async Task GetAllAsync_Should_Contains_Newly_Created_Object()
        {
            //Arrange
            var characterName = "J-Roc";
            var characterFriends = CharacterTestObjects.Characters.Take(2).ToList();
            var characterEpisodes = EpisodesTestsObjects.TestEpisodes;
            var characterRequest = new CharacterRequest
            {
                Episodes = characterEpisodes.Select(s => s.Id).ToArray(),
                Friends = characterFriends.Select(s => s.Id).ToArray(), Name = characterName
            };
            var expectedResponse = new CharacterResponse
            {
                Name = characterName,
                Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Title).ToArray(),
                Friends = characterFriends.Select(s => s.Name).ToArray()
            };
            expectedResponse.SortArrayPropertiesOfType<CharacterResponse, string>();
            var jsonString = JsonConvert.SerializeObject(characterRequest);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await Client.PostAsync(ApiEndpointAddress, content);
            response.EnsureSuccessStatusCode();
            var responseAsString = await response.Content.ReadAsStringAsync();
            var deserializedResponse =
                (CharacterResponse) JsonConvert.DeserializeObject(responseAsString, typeof(CharacterResponse));
            deserializedResponse.SortArrayPropertiesOfType<CharacterResponse, string>();
            expectedResponse.Id = deserializedResponse.Id;

            //Act 
            var deserializedGetAllResponse = await HttpCaller.GetDeserializedResponseFromGetAllAsync();

            //Cleanup
            RecreateDatabase();

            //Assert
            var newlyAddedObject = deserializedGetAllResponse.FirstOrDefault(o => o.Id == deserializedResponse.Id);
            Assert.True(EqualityComparer.Equals(expectedResponse, newlyAddedObject));
        }


        [TestCase]
        public async Task UpdateAsync_Should_Return_Bad_Request_When_Name_Is_Missing()
        {
            //Arrange
            var characterRequest = new CharacterRequest
                {Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Id).ToArray()};
            var jsonString = JsonConvert.SerializeObject(characterRequest);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PutAsync(ApiEndpointAddress, content);
            var responseAsString = await response.Content.ReadAsStringAsync();

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
            Assert.AreEqual(responseAsString,
                "{\"message\":\"Invalid request\",\"details\":\"[\\\"Name is required\\\"]\"}");
        }

        [TestCase]
        public async Task UpdateAsync_Should_Return_Bad_Request_When_Episodes_Are_Missing()
        {
            //Arrange
            var characterRequest = new CharacterRequest
                {Name = "Cory"};
            var jsonString = JsonConvert.SerializeObject(characterRequest);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PutAsync(ApiEndpointAddress, content);
            var responseAsString = await response.Content.ReadAsStringAsync();

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
            Assert.AreEqual(responseAsString,
                "{\"message\":\"Invalid request\",\"details\":\"[\\\"Episodes are required\\\"]\"}");
        }

        [TestCase]
        public async Task UpdateAsync_Should_Return_Bad_Request_When_Episodes_And_Name_Are_Missing()
        {
            //Arrange
            var characterRequest = new CharacterRequest();

            var jsonString = JsonConvert.SerializeObject(characterRequest);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            //Act
            var response = await Client.PutAsync(ApiEndpointAddress, content);
            var responseAsString = await response.Content.ReadAsStringAsync();

            //Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Status code should be 400");
            Assert.AreEqual(responseAsString,
                "{\"message\":\"Invalid request\",\"details\":\"[\\\"Name is required\\\",\\\"Episodes are required\\\"]\"}");
        }


        [TestCase]
        public async Task UpdateAsync_Should_Return_Updated_Object_When_Everything_Is_Correct()
        {
            //Arrange
            var characterName = "Randy";
            var characterFriends = CharacterTestObjects.Characters.Take(2).ToList();
            var characterEpisodes = EpisodesTestsObjects.TestEpisodes;
            var characterId = CharacterTestObjects.Characters.Skip(3).Take(1).FirstOrDefault().Id;
            var characterRequest = new CharacterRequest
            {
                Episodes = characterEpisodes.Select(s => s.Id).ToArray(),
                Friends = characterFriends.Select(s => s.Id).ToArray(),
                Name = characterName,
                Id = characterId
            };

            var expectedResponse = new CharacterResponse
            {
                Name = characterName,
                Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Title).ToArray(),
                Friends = characterFriends.Select(s => s.Name).ToArray(),
                Id = characterId
            };

            var response = await HttpCaller.GetDeserializedResponseFromUpdateAsync(characterRequest);

            //Cleanup
            RecreateDatabase();
            //Assert
            Assert.True(EqualityComparer.Equals(expectedResponse, response));
        }

        [TestCase]
        public async Task GetByIdAsync_Should_Return_New_Values_After_Update()
        {
            //Arrange
            var characterName = "Randy";
            var characterFriends = CharacterTestObjects.Characters.Take(2).ToList();
            var characterEpisodes = EpisodesTestsObjects.TestEpisodes;
            var characterId = CharacterTestObjects.Characters.Skip(3).Take(1).FirstOrDefault().Id;
            var characterRequest = new CharacterRequest
            {
                Episodes = characterEpisodes.Select(s => s.Id).ToArray(),
                Friends = characterFriends.Select(s => s.Id).ToArray(),
                Name = characterName,
                Id = characterId
            };

            var expectedResponse = new CharacterResponse
            {
                Name = characterName,
                Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Title).ToArray(),
                Friends = characterFriends.Select(s => s.Name).ToArray(),
                Id = characterId
            };
            expectedResponse.SortArrayPropertiesOfType<CharacterResponse, string>();


            //Act
            await HttpCaller.GetDeserializedResponseFromUpdateAsync(characterRequest);
            var deserializedResponse = await HttpCaller.GetDeserializedResponseFromGetById(characterId);

            //Cleanup
            RecreateDatabase();

            //Assert
            Assert.True(EqualityComparer.Equals(expectedResponse, deserializedResponse));
        }


        [TestCase]
        public async Task GetAllAsync_Should_Contain_Updated_Object_After_Update()
        {
            //Arrange
            var characterName = "Randy";
            var characterFriends = CharacterTestObjects.Characters.Take(2).ToList();
            var characterEpisodes = EpisodesTestsObjects.TestEpisodes;
            var characterId = CharacterTestObjects.Characters.Skip(3).Take(1).FirstOrDefault().Id;
            var characterRequest = new CharacterRequest
            {
                Episodes = characterEpisodes.Select(s => s.Id).ToArray(),
                Friends = characterFriends.Select(s => s.Id).ToArray(),
                Name = characterName,
                Id = characterId
            };

            var expectedResponse = new CharacterResponse
            {
                Name = characterName,
                Episodes = EpisodesTestsObjects.TestEpisodes.Select(s => s.Title).ToArray(),
                Friends = characterFriends.Select(s => s.Name).ToArray(),
                Id = characterId
            };
            expectedResponse.SortArrayPropertiesOfType<CharacterResponse, string>();

            //Act
            await HttpCaller.GetDeserializedResponseFromUpdateAsync(characterRequest);

            var deserializedResponse = await HttpCaller.GetDeserializedResponseFromGetAllAsync();
            var updatedObject = deserializedResponse.FirstOrDefault(o => o.Id == characterId);
            updatedObject.SortArrayPropertiesOfType<CharacterResponse, string>();
            //Cleanup
            RecreateDatabase();

            //Assert
            Assert.True(EqualityComparer.Equals(expectedResponse, updatedObject));
        }


        [TestCase]
        public async Task UpdateAsync_Should_Update_Relations()
        {
            //Arrange
            var characterName = "Randy";
            var characterFriends = CharacterTestObjects.Characters.Take(4).ToList();
            var characterEpisodes = EpisodesTestsObjects.TestEpisodes.Take(2);
            var characterId = CharacterTestObjects.Characters.Skip(5).Take(1).FirstOrDefault().Id;
            var characterRequest = new CharacterRequest
            {
                Episodes = characterEpisodes.Select(s => s.Id).ToArray(),
                Friends = characterFriends.Select(s => s.Id).ToArray(),
                Name = characterName,
                Id = characterId
            };
            //Act
            await HttpCaller.GetDeserializedResponseFromUpdateAsync(characterRequest);
            var characters = await HttpCaller.GetDeserializedResponseFromGetAllAsync();

            //Assert
            var characterFriendsId = characterFriends.Select(s => s.Id).ToList();
            foreach (var character in characters)
                if (characterFriendsId.Contains(character.Id))
                    Assert.True(character.Friends.Contains(characterName));
                else
                    Assert.False(character.Friends.Contains(characterName));

            var characterEpisodesId = characterEpisodes.Select(s => s.Id).ToList();
            var getAllEpisodesResponse = await EpisodeCaller.GetDeserializedResponseFromGetAllAsync();

            foreach (var episode in getAllEpisodesResponse)
                if (characterEpisodesId.Contains(episode.Id))
                    Assert.True(episode.Characters.Contains(characterName));
                else
                    Assert.False(episode.Characters.Contains(characterName));

            //Cleanup
            RecreateDatabase();
        }
    }
}