﻿using System.Net.Http;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;

namespace StarWarsService.IntegrationTests.Client
{
    public class CharacterCaller : HttpCallerBase<CharacterResponse, CharacterRequest>
    {
        public CharacterCaller(HttpClient client, string baseAddress) : base(client, baseAddress)
        {
        }
    }
}