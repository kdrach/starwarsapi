﻿using System.Net.Http;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;

namespace StarWarsService.IntegrationTests.Client
{
    public class EpisodeCaller : HttpCallerBase<EpisodeResponse, EpisodeRequest>
    {
        public EpisodeCaller(HttpClient client, string baseAddress) : base(client, baseAddress)
        {
        }
    }
}