﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.IntegrationTests.Extensions;

namespace StarWarsService.IntegrationTests.Client
{
    public class HttpCallerBase<T1, T2> where T1 : BaseResponse where T2 : BaseRequest
    {
        private readonly string _apiEndpointAddress;
        private readonly HttpClient _client;

        public HttpCallerBase(HttpClient client, string baseAddress)
        {
            _client = client;
            _apiEndpointAddress = baseAddress;
        }


        public async Task<T1> GetDeserializedResponseFromGetById(Guid objectId)
        {
            var responseFromGetById = await _client.GetAsync(_apiEndpointAddress + $"/{objectId}");
            responseFromGetById.EnsureSuccessStatusCode();
            var responseFromGetByIdAsString = await responseFromGetById.Content.ReadAsStringAsync();
            var deserializedResponse =
                (T1) JsonConvert.DeserializeObject(responseFromGetByIdAsString,
                    typeof(T1));
            deserializedResponse.SortArrayPropertiesOfType<T1, string>();
            return deserializedResponse;
        }

        public async Task<T1> GetDeserializedResponseFromDelete(Guid objectId)
        {
            var responseFromGetById = await _client.DeleteAsync(_apiEndpointAddress + $"/{objectId}");
            responseFromGetById.EnsureSuccessStatusCode();
            var responseFromGetByIdAsString = await responseFromGetById.Content.ReadAsStringAsync();
            var deserializedResponse =
                (T1) JsonConvert.DeserializeObject(responseFromGetByIdAsString,
                    typeof(T1));
            deserializedResponse.SortArrayPropertiesOfType<T1, string>();
            return deserializedResponse;
        }


        public async Task<T1[]> GetDeserializedResponseFromGetAllAsync()
        {
            var responseFromGetAllAsync = await _client.GetAsync(_apiEndpointAddress);
            responseFromGetAllAsync.EnsureSuccessStatusCode();
            var responseFromGetAllAsyncAsString = await responseFromGetAllAsync.Content.ReadAsStringAsync();
            var deserializedResponse =
                (T1[]) JsonConvert.DeserializeObject(responseFromGetAllAsyncAsString,
                    typeof(T1[]));
            deserializedResponse.SortPropertiesInAllObjectsFromResponsesArray();
            return deserializedResponse;
        }

        public async Task<T1> GetDeserializedResponseFromCreateAsync(T2 request)
        {
            var jsonString = JsonConvert.SerializeObject(request);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync(_apiEndpointAddress, content);
            response.EnsureSuccessStatusCode();
            var responseAsString = await response.Content.ReadAsStringAsync();
            var deserializedResponse =
                (T1) JsonConvert.DeserializeObject(responseAsString, typeof(T1));
            deserializedResponse.SortArrayPropertiesOfType<T1, string>();
            return deserializedResponse;
        }


        public async Task<T1> GetDeserializedResponseFromUpdateAsync(T2 request)
        {
            var jsonString = JsonConvert.SerializeObject(request);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await _client.PutAsync(_apiEndpointAddress, content);
            var responseAsString = await response.Content.ReadAsStringAsync();
            var deserializedResponse =
                (T1) JsonConvert.DeserializeObject(responseAsString,
                    typeof(T1));
            return deserializedResponse;
        }
    }
}