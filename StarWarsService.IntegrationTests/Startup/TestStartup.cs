﻿using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StarWarsService.Api.Infrastructure.Middleware;
using StarWarsService.Application.Extensions;
using StarWarsService.Domain.Interfaces;
using StarWarsService.Infrastructure;
using StarWarsService.Infrastructure.Repositories;
using StarWarsService.IntegrationTests.Extensions;

namespace StarWarsService.IntegrationTests.Startup
{
    public class TestStartup : Api.Startup
    {
        public TestStartup(IConfiguration configuration) : base(configuration)
        {
        }


        public override void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureTestDatabaseConnection(Configuration);
            services.ConfigureApplicationServices();
            services.AddScoped<ICharactersRepository, CharactersRepository>();
            services.AddScoped<IEpisodeRepository, EpisodeRepository>();
            services.AddControllers().AddApplicationPart(Assembly.Load(new AssemblyName("StarWarsService.Api")));
        }

        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var exceptionMiddleware = new SerializedExceptionMiddleware(
                app.ApplicationServices.GetRequiredService<IWebHostEnvironment>());

            app.UseExceptionHandler(new ExceptionHandlerOptions {ExceptionHandler = exceptionMiddleware.Invoke});

            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}