﻿using StarWarsService.Infrastructure;
using StarWarsService.IntegrationTests.ValueTypes;

namespace StarWarsService.IntegrationTests.Database
{
    public class DatabaseTestUtilities
    {
        public static void Seed(StarWarsContext context)
        {
            context.AddRange(EpisodesTestsObjects.TestEpisodes);
            context.AddRange(CharacterTestObjects.Characters);
            context.AddRange(CharacterTestObjects.CharacterEpisodes);
            context.AddRange(CharacterTestObjects.CharacterFriends);
            context.SaveChanges();
        }
    }
}