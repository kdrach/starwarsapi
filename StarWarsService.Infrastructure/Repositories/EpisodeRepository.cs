﻿using StarWarsService.Domain.Interfaces;
using StarWarsService.Domain.Models;

namespace StarWarsService.Infrastructure.Repositories
{
    public class EpisodeRepository : GenericRepository<Episode>, IEpisodeRepository
    {
        public EpisodeRepository(StarWarsContext context) : base(context)
        {
        }
    }
}