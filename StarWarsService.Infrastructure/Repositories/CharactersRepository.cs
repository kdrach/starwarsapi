﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StarWarsService.Domain.Interfaces;
using StarWarsService.Domain.Models;
using StarWarsService.Infrastructure.Pagination;

namespace StarWarsService.Infrastructure.Repositories
{
    public class CharactersRepository : GenericRepository<Character>, ICharactersRepository
    {
        public CharactersRepository(StarWarsContext context) : base(context)
        {
        }

        public async Task<PagedList<Character>> GetAll(PaginationOptions parameters)
        {
            return await PagedListOperations<Character>.ToPagedList(Context.Set<Character>()
                .Include(Context.GetIncludePaths(typeof(Character))).Include(c => c.CharacterFriends)
                .ThenInclude(cf => cf.Friend)
                .AsQueryable(), parameters.PageNumber, parameters.PageSize);
        }

        public virtual async Task<Character> GetById(Guid id)
        {
            return await Context.Set<Character>().Include(Context.GetIncludePaths(typeof(Character))).AsQueryable()
                .Include(c => c.CharacterFriends)
                .ThenInclude(cf => cf.Friend).SingleOrDefaultAsync(e => e.Id == id);
        }
    }
}