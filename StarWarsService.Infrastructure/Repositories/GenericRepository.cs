﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StarWarsService.Domain.Interfaces;
using StarWarsService.Domain.Models;
using StarWarsService.Infrastructure.Pagination;

namespace StarWarsService.Infrastructure.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected StarWarsContext Context;


        public GenericRepository(StarWarsContext context)
        {
            Context = context;
        }

        public virtual async Task<T> GetById(Guid id)
        {
            return await Context.Set<T>().Include(Context.GetIncludePaths(typeof(T))).AsQueryable()
                .SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task Add(T entity)
        {
            await Context.Set<T>().AddAsync(entity);
            await Context.SaveChangesAsync();
        }

        public Task Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            return Context.SaveChangesAsync();
        }

        public Task Remove(T entity)
        {
            Context.Set<T>().Remove(entity);
            return Context.SaveChangesAsync();
        }

        public virtual async Task<PagedList<T>> GetAll(PaginationOptions parameters)
        {
            return await PagedListOperations<T>.ToPagedList(
                Context.Set<T>().Include(Context.GetIncludePaths(typeof(T))).AsQueryable(), parameters.PageNumber,
                parameters.PageSize);
        }
    }
}