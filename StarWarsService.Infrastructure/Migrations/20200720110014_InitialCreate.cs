﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StarWarsService.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Characters",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Characters", x => x.Id); });

            migrationBuilder.CreateTable(
                "Episodes",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Episodes", x => x.Id); });

            migrationBuilder.CreateTable(
                "CharacterFriends",
                table => new
                {
                    CharacterId = table.Column<Guid>(),
                    FriendId = table.Column<Guid>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterFriends", x => new {x.CharacterId, x.FriendId});
                    table.ForeignKey(
                        "FK_CharacterFriends_Characters_CharacterId",
                        x => x.CharacterId,
                        "Characters",
                        "Id");
                    table.ForeignKey(
                        "FK_CharacterFriends_Characters_FriendId",
                        x => x.FriendId,
                        "Characters",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "CharactersEpisodes",
                table => new
                {
                    CharacterId = table.Column<Guid>(),
                    EpisodeId = table.Column<Guid>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharactersEpisodes", x => new {x.CharacterId, x.EpisodeId});
                    table.ForeignKey(
                        "FK_CharactersEpisodes_Characters_CharacterId",
                        x => x.CharacterId,
                        "Characters",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_CharactersEpisodes_Episodes_EpisodeId",
                        x => x.EpisodeId,
                        "Episodes",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                "IX_CharacterFriends_FriendId",
                "CharacterFriends",
                "FriendId");

            migrationBuilder.CreateIndex(
                "IX_CharactersEpisodes_EpisodeId",
                "CharactersEpisodes",
                "EpisodeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "CharacterFriends");

            migrationBuilder.DropTable(
                "CharactersEpisodes");

            migrationBuilder.DropTable(
                "Characters");

            migrationBuilder.DropTable(
                "Episodes");
        }
    }
}