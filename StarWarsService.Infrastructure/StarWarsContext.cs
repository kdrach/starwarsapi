﻿using Microsoft.EntityFrameworkCore;
using StarWarsService.Domain.Models;

namespace StarWarsService.Infrastructure
{
    public class StarWarsContext : DbContext
    {
        public StarWarsContext()
        {
        }

        public StarWarsContext(DbContextOptions<StarWarsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Episode> Episodes { get; set; }
        public virtual DbSet<Character> Characters { get; set; }
        public virtual DbSet<CharacterFriend> CharacterFriend { get; set; }
        public virtual DbSet<CharacterEpisode> CharacterEpisode { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>()
                .ToTable("Characters");

            modelBuilder.Entity<Episode>()
                .ToTable("Episodes");

            modelBuilder.Entity<CharacterFriend>().ToTable("CharacterFriends")
                .HasKey(cf => new {cf.CharacterId, cf.FriendId});

            modelBuilder.Entity<CharacterFriend>()
                .HasOne(cf => cf.Friend)
                .WithMany(c => c.FriendOf)
                .HasForeignKey(cf => cf.FriendId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CharacterFriend>()
                .HasOne(cf => cf.Character)
                .WithMany(c => c.CharacterFriends)
                .HasForeignKey(cf => cf.CharacterId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CharacterEpisode>().ToTable("CharactersEpisodes")
                .HasKey(ce => new {ce.CharacterId, ce.EpisodeId});

            modelBuilder.Entity<CharacterEpisode>()
                .HasOne(ce => ce.Character)
                .WithMany(c => c.CharacterEpisodes)
                .HasForeignKey(ce => ce.CharacterId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CharacterEpisode>()
                .HasOne(ce => ce.Episode)
                .WithMany(e => e.CharacterEpisodes)
                .HasForeignKey(ce => ce.EpisodeId)
                .OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(modelBuilder);
        }
    }
}