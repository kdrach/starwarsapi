﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Moq;
using NUnit.Framework;
using StarWarsService.Api.Controllers;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Interfaces;
using StarWarsService.Domain.Models;
using StarWarsService.Domain.ValueTypes;
using StarWarsService.UnitTests.Helpers;

namespace StarWarsService.UnitTests.Api.Controllers
{
    [TestFixture]
    public class EpisodeControllerTests : BaseControllerTests<EpisodeRequest, EpisodeResponse>
    {
        //Arrange
        [SetUp]
        public virtual void Setup()
        {
            ServiceMock = new Mock<IGenericService<EpisodeRequest, EpisodeResponse>>();

            var serviceFactoryMock = new Mock<IServiceFactory<EpisodeRequest, EpisodeResponse>>();
            ServiceMock.Setup(c => c.DeleteAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).CallBase();

            serviceFactoryMock.Setup(c => c.Create(ControllerEnum.EpisodeController)).Returns(ServiceMock.Object);
            BaseController = new EpisodeController(serviceFactoryMock.Object);

            ServiceMock.Setup(_ => _.GetAsync(TestsConstants.Episodes.First().Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(TestsConstants.EpisodeResponses.First);

            ServiceMock.Setup(_ => _.GetAllAsync(It.IsAny<CancellationToken>(), It.IsAny<PaginationOptions>()))
                .ReturnsAsync(new PagedList<EpisodeResponse>(TestsConstants.EpisodeResponses.ToList(),
                    TestsConstants.EpisodeResponses.Length, TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize));
        }

        public override List<EpisodeResponse> ObjectList => TestsConstants.EpisodeResponses.ToList();

        public override Guid ExistingObjectGuid => TestsConstants.Episodes.First().Id;
        public override EpisodeRequest ServiceRequest => TestsConstants.EpisodeRequest;
        public override EpisodeResponse ServiceResponse => TestsConstants.EpisodeResponses.First();
        public override EpisodeRequest BadRequest => new EpisodeRequest {Id = Guid.Empty};
    }
}