﻿using System;
using System.Collections.Generic;
using System.Threading;
using Moq;
using NUnit.Framework;
using StarWarsService.Api.Controllers;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Interfaces;
using StarWarsService.Domain.Models;
using StarWarsService.Domain.ValueTypes;
using StarWarsService.UnitTests.Helpers;

namespace StarWarsService.UnitTests.Api.Controllers
{
    [TestFixture]
    public class CharacterControllerTests : BaseControllerTests<CharacterRequest, CharacterResponse>
    {
        [SetUp]
        public void SetUp()
        {
            ServiceMock = new Mock<IGenericService<CharacterRequest, CharacterResponse>>();
            ServiceMock.Setup(c => c.DeleteAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).CallBase();

            var serviceFactoryMock = new Mock<IServiceFactory<CharacterRequest, CharacterResponse>>();
            serviceFactoryMock.Setup(c => c.Create(ControllerEnum.CharacterController)).Returns(ServiceMock.Object);
            BaseController = new CharacterController(serviceFactoryMock.Object);

            ServiceMock.Setup(_ => _.GetAsync(TestsConstants.ExistingObjectGuid, It.IsAny<CancellationToken>()))
                .ReturnsAsync(TestsConstants.TestCharacterResponse);

            ServiceMock.Setup(_ => _.GetAllAsync(It.IsAny<CancellationToken>(), It.IsAny<PaginationOptions>()))
                .ReturnsAsync(new PagedList<CharacterResponse>(TestsConstants.CharactersResponses,
                    TestsConstants.CharactersResponses.Count, TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize));
        }

        public override List<CharacterResponse> ObjectList => TestsConstants.CharactersResponses;
        public override Guid ExistingObjectGuid => TestsConstants.ExistingObjectGuid;
        public override CharacterRequest ServiceRequest => TestsConstants.CharacterRequest;
        public override CharacterResponse ServiceResponse => TestsConstants.TestCharacterResponse;
        public override CharacterRequest BadRequest => new CharacterRequest {Id = Guid.Empty};
    }
}