﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using StarWarsService.Api.Controllers;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Interfaces;
using StarWarsService.Domain.Models;
using StarWarsService.UnitTests.Helpers;

namespace StarWarsService.UnitTests.Api.Controllers
{
    public abstract class BaseControllerTests<T1, T2> where T1 : BaseRequest where T2 : BaseResponse
    {
        //These properties are setuped in derrived classes
        protected BaseController<T1, T2> BaseController { get; set; }
        protected Mock<IGenericService<T1, T2>> ServiceMock { get; set; }
        public abstract List<T2> ObjectList { get; }
        public abstract Guid ExistingObjectGuid { get; }
        public abstract T1 ServiceRequest { get; }
        public abstract T2 ServiceResponse { get; }
        public abstract T1 BadRequest { get; }


        [Test]
        public async Task GetAllAsync_Response_Should_Not_Be_Null()
        {
            //Act
            var response =
                await BaseController.GetAllAsync(new CancellationToken(), TestsConstants.DefaultGetAllAsyncParameters);

            //Assert
            Assert.IsNotNull(response, "Response should not be null");
        }

        [Test]
        public async Task GetAllAsync_Response_Should_Be_Instance_Of_ObjectResult()
        {
            //Act
            var response =
                await BaseController.GetAllAsync(new CancellationToken(), TestsConstants.DefaultGetAllAsyncParameters);

            //Assert
            Assert.IsInstanceOf(typeof(ObjectResult), response, "Response should be instance of ObjectResult");
        }

        [Test]
        public async Task GetAllAsyncc_Response_Value_Should_Not_Be_Null()
        {
            //Act
            var response =
                await BaseController.GetAllAsync(new CancellationToken(), TestsConstants.DefaultGetAllAsyncParameters)
                    as OkObjectResult;

            //Assert
            Assert.IsNotNull(response.Value, "Response Value should not be null");
        }

        [Test]
        public async Task GetAllAsync_Response_Should_Be_Ok()
        {
            //Act
            var response =
                await BaseController.GetAllAsync(new CancellationToken(), TestsConstants.DefaultGetAllAsyncParameters)
                    as OkObjectResult;

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOf(typeof(OkObjectResult), response, "Response should be 200");
            Assert.IsNotNull(response.StatusCode, "StatusCode should not be null");
            Assert.AreEqual((int) HttpStatusCode.OK, response.StatusCode, "Status code should be 200");
        }


        [Test]
        public async Task GetAllAsync_Response_Value_Should_Be_List_Of_Characters()
        {
            //Act
            var response =
                await BaseController.GetAllAsync(new CancellationToken(), TestsConstants.DefaultGetAllAsyncParameters)
                    as OkObjectResult;

            //Assert
            Assert.IsInstanceOf(typeof(PagedList<T2>), response.Value,
                $"Response value should be instance of PagedList<{typeof(T2)}>");
        }

        [Test]
        public async Task GetAllAsync_Response_Value_Should_Be_As_Expected()
        {
            //Act
            var response =
                await BaseController.GetAllAsync(new CancellationToken(), TestsConstants.DefaultGetAllAsyncParameters)
                    as OkObjectResult;
            var value = response.Value as List<T2>;

            //Assert
            Assert.AreEqual(ObjectList, value, "Response value should be the same as expected");
        }

        [Test]
        public async Task GetAllAsync_Should_Return_Empty_List_When_There_Is_No_Element_In_Database()
        {
            //Arrange
            var emptyResponse = Enumerable.Empty<T2>().ToList();
            ServiceMock.Setup(_ => _.GetAllAsync(It.IsAny<CancellationToken>(), It.IsAny<PaginationOptions>()))
                .ReturnsAsync(new PagedList<T2>(emptyResponse, 0, TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize));

            //Act
            var response =
                await BaseController.GetAllAsync(new CancellationToken(), TestsConstants.DefaultGetAllAsyncParameters)
                    as OkObjectResult;
            var value = response.Value as List<T2>;

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOf(typeof(OkObjectResult), response, "Response should be 200");
            Assert.IsNotNull(response.StatusCode, "StatusCode should not be null");
            Assert.AreEqual((int) HttpStatusCode.OK, response.StatusCode, "Status Code should be 200");
            Assert.AreEqual(new List<T2>(), value, "Method should return empty list");
        }


        [TestCaseSource(typeof(TestCaseDataSources),
            nameof(TestCaseDataSources.GetTestCaseDataForExistingAndNonExistingObject))]
        public async Task GetByIdAsync_Response_Should_Not_Be_Null(Guid characterGuid)
        {
            //Arrange
            var requestParameters = new GetByIdParameters {Id = characterGuid};

            //Act
            var response = await BaseController.GetByIdAsync(requestParameters, new CancellationToken());

            //Assert
            Assert.IsNotNull(response, "Response Value should not be null");
        }

        [Test]
        public async Task GetByIdAsync_When_Object_Exists_Response_Status_Should_Be_OK()
        {
            //Arrange
            var requestParameters = new GetByIdParameters {Id = ExistingObjectGuid};

            //Act
            var response =
                await BaseController.GetByIdAsync(requestParameters, new CancellationToken()) as OkObjectResult;

            //Assert
            Assert.AreEqual((int) HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async Task GetByIdAsync_When_Object_Exists_Response_Status_Should_Be_Not_Found()
        {
            //Arrange
            var requestParameters = new GetByIdParameters {Id = TestsConstants.NonExistingObjectGuid};

            //Act
            var response =
                await BaseController.GetByIdAsync(requestParameters, new CancellationToken()) as
                    NotFoundResult;

            //Assert
            Assert.AreEqual((int) HttpStatusCode.NotFound, response.StatusCode, "Status code should be Not Found");
        }

        [Test]
        public async Task GetByIdAsync_When_Id_Is_Empty_Response_Status_Should_Be_Not_Found()
        {
            //Arrange
            var requestParameters = new GetByIdParameters {Id = new Guid()};

            //Act
            var response =
                await BaseController.GetByIdAsync(requestParameters, new CancellationToken()) as
                    NotFoundResult;

            //Assert
            Assert.AreEqual((int) HttpStatusCode.NotFound, response.StatusCode, "Status code should be Not Found");
        }

        [Test]
        public async Task GetAllAsync_Response_When_Object_Exists_Response_Status_Code_Should_Be_OK()
        {
            //Act
            var response =
                await BaseController.GetAllAsync(new CancellationToken(), TestsConstants.DefaultGetAllAsyncParameters)
                    as OkObjectResult;

            //Assert
            Assert.AreEqual((int) HttpStatusCode.OK, response.StatusCode, "Response status code should be OK");
        }

        [Test]
        public async Task GetByIdAsync_Response_Should_Be_Same_As_Expected()
        {
            //Arrange
            var requestParameters = new GetByIdParameters {Id = ExistingObjectGuid};
            //Act
            var response =
                await BaseController.GetByIdAsync(requestParameters, new CancellationToken()) as OkObjectResult;
            var value = response.Value as T2;
            //Assert
            Assert.AreEqual(ServiceResponse, value, "Response value should be the same as expected");
        }

        [Test]
        public async Task DeleteAsync_It_Should_Return_Bad_Request_When_Object_Not_Exists_In_Database()
        {
            //Arrange
            ServiceMock.Setup(c => c.DeleteAsync(TestsConstants.NonExistingObjectGuid, It.IsAny<CancellationToken>()))
                .Throws<InvalidOperationException>();
            var requestParameters = new DeleteByIdParameters {Id = TestsConstants.NonExistingObjectGuid};
            //Act
            var response = await BaseController.DeleteAsync(requestParameters, new CancellationToken());

            //Assert
            Assert.IsInstanceOf(typeof(BadRequestObjectResult), response,
                "Response should be instance of BadRequestResult");
        }

        [Test]
        public async Task DeleteAsync_It_Should_Return_Not_Found_When_Id_Is_Empty()
        {
            //Arrange
            ServiceMock.Setup(c => c.DeleteAsync(ServiceResponse.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResponse);
            var requestParameters = new DeleteByIdParameters {Id = Guid.Empty};
            //Act
            var response = await BaseController.DeleteAsync(requestParameters, new CancellationToken());

            //Assert
            Assert.IsInstanceOf(typeof(NotFoundResult), response, "Response should be instance of NotFoundResult");
        }


        [Test]
        public async Task DeleteAsync_It_Should_Return_OK_When_Object_Exists_In_Database()
        {
            //Arrange
            ServiceMock.Setup(c => c.DeleteAsync(ServiceResponse.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResponse);
            var requestParameters = new DeleteByIdParameters {Id = ServiceResponse.Id};
            //Act
            var response = await BaseController.DeleteAsync(requestParameters, new CancellationToken());

            //Assert
            Assert.IsInstanceOf(typeof(OkObjectResult), response, "Response should be instance of OkObjectResult");
            var value = (response as OkObjectResult).Value;
            Assert.AreEqual(ServiceResponse, value);
        }

        [Test]
        public async Task CreateAsync_It_Should_Return_Bad_Request_When_Service_Throws_Exception()
        {
            //Arrange
            ServiceMock.Setup(c => c.CreateAsync(ServiceRequest, It.IsAny<CancellationToken>()))
                .Throws<InvalidOperationException>();

            //Act
            var response = await BaseController.CreateAsync(ServiceRequest, new CancellationToken());

            //Assert
            Assert.IsInstanceOf(typeof(BadRequestObjectResult), response,
                "Response should be instance of BadRequestResult");
        }

        [Test]
        public async Task CreateAsync_It_Should_Return_Ok_When_Service_Not_Throw_Exception()
        {
            //Arrange
            ServiceMock.Setup(c => c.CreateAsync(ServiceRequest, It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResponse);

            //Act
            var response = await BaseController.CreateAsync(ServiceRequest, new CancellationToken());

            //Assert
            Assert.IsInstanceOf(typeof(OkObjectResult), response, "Response should be instance of OkObjectResult");
            var value = (response as OkObjectResult).Value;
            Assert.AreEqual(ServiceResponse, value);
        }


        [Test]
        public async Task UpdateAsync_It_Should_Return_Bad_Request_When_Service_Throws_Exception()
        {
            //Arrange
            ServiceMock.Setup(c => c.UpdateAsync(ServiceRequest, It.IsAny<CancellationToken>()))
                .Throws<InvalidOperationException>();

            //Act
            var response = await BaseController.UpdateAsync(ServiceRequest, new CancellationToken());

            //Assert
            Assert.IsInstanceOf(typeof(BadRequestObjectResult), response,
                "Response should be instance of BadRequestResult");
        }

        [Test]
        public async Task UpdateAsync_It_Should_Return_Ok_When_Service_Not_Throw_Exception()
        {
            //Arrange
            ServiceMock.Setup(c => c.UpdateAsync(ServiceRequest, It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResponse);

            //Act
            var response = await BaseController.UpdateAsync(ServiceRequest, new CancellationToken());

            //Assert
            Assert.IsInstanceOf(typeof(OkObjectResult), response, "Response should be instance of OkObjectResult");
            var value = (response as OkObjectResult).Value;
            Assert.AreEqual(ServiceResponse, value);
        }

        [Test]
        public async Task UpdateAsync_It_Should_Return_Bad_Request_When_Request_Not_Contains_Id()
        {
            //Arrange
            ServiceMock.Setup(c => c.UpdateAsync(BadRequest, It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResponse);

            //Act
            var response = await BaseController.UpdateAsync(BadRequest, new CancellationToken());

            //Assert
            Assert.IsInstanceOf(typeof(BadRequestObjectResult), response,
                "Response should be instance of BadRequestObjectResult");
            var value = (response as BadRequestObjectResult).Value;
            Assert.AreEqual("Id is required when updating object", value);
        }
    }
}