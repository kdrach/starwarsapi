﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using StarWarsService.Api.Infrastructure.Middleware;
using StarWarsService.Api.Models;

namespace StarWarsService.UnitTests.Api.Infrastructure
{
    [TestFixture]
    public class SerializedExceptionMiddlewareTests
    {
        [SetUp]
        public void SetUp()
        {
            _webHostEnvironmentMock = new Mock<IWebHostEnvironment>();
        }

        private Mock<IWebHostEnvironment> _webHostEnvironmentMock;
        private readonly string _defaultExceptionMessage = "Default Test Error";
        private readonly string _defaultExceptionStackTrace = "Default StackTrace";


        [TestCase(nameof(Environments.Development))]
        [TestCase(nameof(Environments.Production))]
        [TestCase(nameof(Environments.Staging))]
        public async Task When_Exception_Does_Not_Occurs_Nothing_Should_Be_Returned(string environments)
        {
            // Arrange
            _webHostEnvironmentMock.Setup(m => m.EnvironmentName).Returns(environments);

            var middleware = new SerializedExceptionMiddleware(_webHostEnvironmentMock.Object);

            var context = new DefaultHttpContext();
            context.Response.Body = new MemoryStream();

            //Act
            await middleware.Invoke(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);

            var reader = new StreamReader(context.Response.Body);
            var streamText = reader.ReadToEnd();

            //Assert 
            Assert.AreEqual((int) HttpStatusCode.InternalServerError, context.Response.StatusCode);
            Assert.AreEqual(string.Empty, streamText);

            reader.Dispose();
        }


        [TestCase(nameof(Environments.Development))]
        [TestCase(nameof(Environments.Staging))]
        public async Task When_Exception_Occurs_On_Dev_Or_Stage_Environment_Details_Should_Be_Returned(
            string environment)
        {
            // Arrange
            _webHostEnvironmentMock.Setup(m => m.EnvironmentName).Returns(environment);

            var middleware = new SerializedExceptionMiddleware(_webHostEnvironmentMock.Object);
            var exceptionHandlerFeatureMock = new Mock<IExceptionHandlerFeature>();
            CreateAndSetupMockedException();

            exceptionHandlerFeatureMock.Setup(e => e.Error).Returns(CreateAndSetupMockedException);
            var context = new DefaultHttpContext();
            context.Features.Set(exceptionHandlerFeatureMock.Object);
            context.Response.Body = new MemoryStream();
            //Act
            await middleware.Invoke(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(context.Response.Body);
            var streamText = reader.ReadToEnd();
            var responseObject = JsonConvert.DeserializeObject<ApiError>(streamText);

            //Assert
            Assert.AreEqual((int) HttpStatusCode.InternalServerError, context.Response.StatusCode);
            Assert.AreEqual(_defaultExceptionStackTrace, responseObject.Details);
            Assert.AreEqual(_defaultExceptionMessage, responseObject.Message);

            reader.Dispose();
        }

        private Exception CreateAndSetupMockedException()
        {
            var exceptionMock = new Mock<Exception>();
            exceptionMock.Setup(ex => ex.Message).Returns(_defaultExceptionMessage);
            exceptionMock.Setup(ex => ex.StackTrace).Returns(_defaultExceptionStackTrace);
            return exceptionMock.Object;
        }

        [Test]
        public async Task When_Exception_Occurs_On_Prod_Environment_Generic_Message_Should_Be_Returned()
        {
            // Arrange
            _webHostEnvironmentMock.Setup(m => m.EnvironmentName).Returns(Environments.Production);

            var middleware = new SerializedExceptionMiddleware(_webHostEnvironmentMock.Object);
            var exceptionHandlerFeatureMock = new Mock<IExceptionHandlerFeature>();
            CreateAndSetupMockedException();

            exceptionHandlerFeatureMock.Setup(e => e.Error).Returns(CreateAndSetupMockedException);
            var context = new DefaultHttpContext();
            context.Features.Set(exceptionHandlerFeatureMock.Object);
            context.Response.Body = new MemoryStream();
            //Act
            await middleware.Invoke(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(context.Response.Body);
            var streamText = reader.ReadToEnd();
            var responseObject = JsonConvert.DeserializeObject<ApiError>(streamText);

            //Assert
            Assert.AreEqual((int) HttpStatusCode.InternalServerError, context.Response.StatusCode);
            Assert.AreEqual(_defaultExceptionMessage, responseObject.Details);
            Assert.AreEqual(SerializedExceptionMiddleware.DefaultErrorMessage, responseObject.Message);

            reader.Dispose();
        }
    }
}