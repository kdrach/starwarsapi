﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using NUnit.Framework;
using StarWarsService.Application.Characters;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Mapper;
using StarWarsService.Domain.Interfaces;
using StarWarsService.Domain.Models;
using StarWarsService.Tests.Shared.Helpers;
using StarWarsService.UnitTests.Helpers;

namespace StarWarsService.UnitTests.Application.Services
{
    [TestFixture]
    public class CharacterServiceTests
    {
        //Arrange
        [SetUp]
        public void SetUp()
        {
            _characterRepositoryMock = new Mock<ICharactersRepository>();
            _episodesRepositoryMock = new Mock<IEpisodeRepository>();

            var config = new MapperConfiguration(cfg => { cfg.AddProfile(typeof(DefaultAutoMappingProfile)); });
            _mapper = config.CreateMapper();
            _service = new CharacterService(_characterRepositoryMock.Object, _mapper, _episodesRepositoryMock.Object);
        }

        private Mock<ICharactersRepository> _characterRepositoryMock;
        private Mock<IEpisodeRepository> _episodesRepositoryMock;
        private IMapper _mapper;
        private CharacterService _service;

        private void AddMoviesToRepositoryMock()
        {
            foreach (var movie in TestsConstants.Episodes)
                _episodesRepositoryMock.Setup(m => m.GetById(movie.Id)).ReturnsAsync(movie);
        }

        [Test]
        public async Task
            Delete_Character_When_Character_Exists_In_Database_It_Should_Not_Throw_Exception_And_Return_Deleted_Object()
        {
            //Arrange
            var friendCharacterCopy = TestsConstants.FriendCharacter.Copy();
            var testCharacterCopy = TestsConstants.TestCharacter.Copy();

            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.TestCharacter.Id))
                .ReturnsAsync(testCharacterCopy);

            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(friendCharacterCopy);

            //Act 
            var response = await _service.DeleteAsync(TestsConstants.TestCharacter.Id,
                new CancellationToken());

            //Assert
            var testEntityWithoutFriendsRelation = TestsConstants.TestCharacterResponse.Copy();
            testEntityWithoutFriendsRelation.Friends = new string [0];
            Assert.True(new CharacterResponseEqualityComparer().Equals(testEntityWithoutFriendsRelation, response));
        }


        [Test]
        public void Delete_Character_When_Character_Not_Exists_In_Database_It_Should_Throw_Exception()
        {
            //Arrange
            var notExistingCharacterId = TestsConstants.NonExistingObjectGuid;

            //Act & Assert
            Assert.ThrowsAsync<InvalidOperationException>(
                async () => await _service.DeleteAsync(notExistingCharacterId, new CancellationToken()),
                $"Character with id : {notExistingCharacterId} does not exist in database");
        }

        [Test]
        public async Task Delete_Should_Remove_All_Character_Friends_Relation()
        {
            //Arrange 
            var friendCharacterCopy = TestsConstants.FriendCharacter.Copy();
            var testCharacterCopy = TestsConstants.TestCharacter.Copy();

            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(friendCharacterCopy);

            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.TestCharacter.Id))
                .ReturnsAsync(testCharacterCopy);

            //Act
            await _service.DeleteAsync(TestsConstants.TestCharacter.Id, new CancellationToken());

            //Assert
            var friendOfDeletedCharacter =
                await _service.GetAsync(TestsConstants.FriendCharacter.Id, new CancellationToken());
            Assert.True(!friendOfDeletedCharacter.Friends.Contains(TestsConstants.TestCharacter.Name));
        }

        [Test]
        public async Task Get_Character_When_It_Not_Exists_In_Database_It_Should_Return_Null()
        {
            //Act
            var character =
                await _service.GetAsync(TestsConstants.NonExistingObjectGuid, new CancellationToken());
            //Assert
            Assert.IsNull(character);
        }


        [Test]
        public async Task GetAll_When_There_Are_Characters_In_Database_It_Should_Return_List()
        {
            //Arrange
            _characterRepositoryMock.Setup(s => s.GetAll(It.IsAny<PaginationOptions>()))
                .ReturnsAsync(new PagedList<Character>(TestsConstants.CharacterEntitiesList,
                    TestsConstants.CharacterEntitiesList.Count, TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize));

            //Act
            var list = await _service.GetAllAsync(new CancellationToken(), TestsConstants.DefaultPaginationOptions);

            //Assert
            var expectedResponse = new PagedList<CharacterResponse>(TestsConstants.CharactersResponses,
                TestsConstants.CharactersResponses.Count, TestsConstants.DefaultPaginationOptions.PageNumber,
                TestsConstants.DefaultPaginationOptions.PageSize);

            Assert.AreEqual(expectedResponse.CurrentPage, list.CurrentPage);
            Assert.AreEqual(expectedResponse.HasNext, list.HasNext);
            Assert.AreEqual(expectedResponse.PageSize, list.PageSize);
            Assert.AreEqual(expectedResponse.TotalCount, list.TotalCount);
            Assert.AreEqual(expectedResponse.TotalPages, list.TotalPages);
            Assert.AreEqual(expectedResponse.HasPrevious, list.HasPrevious);
            Assert.True(expectedResponse.ToList()
                .SequenceEqual(list.ToList(), new CharacterResponseEqualityComparer()));
        }

        [Test]
        public async Task GetAll_When_There_Are_Not_Characters_In_Database_It_Should_Return_Empty_List()
        {
            //Arrange
            var emptyEntity = Enumerable.Empty<Character>().ToList();
            _characterRepositoryMock.Setup(m => m.GetAll(It.IsAny<PaginationOptions>()))
                .ReturnsAsync(new PagedList<Character>(emptyEntity, emptyEntity.Count,
                    TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize));
            var emptyResponse = Enumerable.Empty<CharacterResponse>().ToList();

            var expectedResponse = new PagedList<CharacterResponse>(emptyResponse, emptyResponse.Count,
                TestsConstants.DefaultPaginationOptions.PageNumber, TestsConstants.DefaultPaginationOptions.PageSize);

            //Act
            var list = await _service.GetAllAsync(new CancellationToken(),
                TestsConstants.DefaultPaginationOptions);

            //Assert
            Assert.AreEqual(expectedResponse, list);
        }

        [Test]
        public async Task GetCharacter_When_It_Exists_In_Database_It_Should_Return_Object()
        {
            //Arrange
            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.TestCharacter.Id))
                .ReturnsAsync(TestsConstants.TestCharacter);

            //Act
            var character =
                await _service.GetAsync(TestsConstants.TestCharacter.Id, new CancellationToken());
            //Assert
            Assert.IsTrue(new CharacterResponseEqualityComparer().Equals(
                _mapper.Map<Character, CharacterResponse>(TestsConstants.TestCharacter), character));
        }

        [Test]
        public async Task Save_Character_Should_Return_Newly_Created_Object()
        {
            //Arrange
            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(TestsConstants.FriendCharacter);
            AddMoviesToRepositoryMock();

            //Act 
            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.TestCharacterResponse.Name,
                Episodes = TestsConstants.Episodes.Select(m => m.Id).ToArray(),
                Friends = new[] {TestsConstants.FriendCharacter.Id}
            };

            // Act 
            var response = await _service.CreateAsync(characterRequest, new CancellationToken());

            //Assert
            Assert.AreEqual(TestsConstants.TestCharacterResponse.Name, response.Name);
            Assert.IsTrue(TestsConstants.Episodes.Select(s => s.Title).SequenceEqual(response.Episodes));
            Assert.AreEqual(TestsConstants.FriendCharacter.Name, response.Friends[0]);
        }


        [Test]
        public void Save_Character_Should_Throw_Exception_When_Episode_Not_Exist_In_Database()
        {
            //Arrange
            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(TestsConstants.FriendCharacter);
            var notExistingMovieId = Guid.NewGuid();

            //Act 
            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.TestCharacterResponse.Name,
                Episodes = new[] {notExistingMovieId},
                Friends = new[] {TestsConstants.FriendCharacter.Id}
            };

            // Act & Assert
            Assert.ThrowsAsync<InvalidOperationException>(
                async () => await _service.CreateAsync(characterRequest, new CancellationToken()),
                $"Movie with id : {notExistingMovieId} does not exist in database");
        }

        [Test]
        public void Save_Character_When_There_Is_No_Friend_Character_With_Specified_Id_Should_Throw_Exception()
        {
            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.TestCharacterResponse.Name,
                Episodes = TestsConstants.TestCharacter.CharacterEpisodes.Select(s => s.EpisodeId).ToArray(),
                Friends = new[] {TestsConstants.NonExistingObjectGuid}
            };

            // Act & Assert
            Assert.ThrowsAsync<InvalidOperationException>(
                async () => await _service.CreateAsync(characterRequest, new CancellationToken()),
                $"Character with id : {TestsConstants.NonExistingObjectGuid} does not exist in database");
        }

        [Test]
        public async Task Update_Character_Should_Not_Throw_Exception_And_Return_Updated_Object()
        {
            //Arrange
            var friendCharacterCopy = TestsConstants.FriendCharacter.Copy();
            var testCharacterCopy = TestsConstants.TestCharacter.Copy();

            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.TestCharacter.Id))
                .ReturnsAsync(testCharacterCopy);
            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(friendCharacterCopy);
            AddMoviesToRepositoryMock();

            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.FriendCharacter.Name,
                Episodes = TestsConstants.FriendCharacter.CharacterEpisodes.Select(s => s.EpisodeId).ToArray(),
                Friends = new[] {TestsConstants.TestCharacter.Id},
                Id = TestsConstants.FriendCharacter.Id
            };
            // Act
            var response = await _service.UpdateAsync(characterRequest, new CancellationToken());

            //Assert
            Assert.IsTrue(
                new CharacterResponseEqualityComparer().Equals(TestsConstants.FriendCharacterResponse, response));
        }


        [Test]
        public void Update_Character_Should_Throw_Exception_When_Episode_Not_Exists_In_Database()
        {
            //Arrange
            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(TestsConstants.FriendCharacter);
            var notExistingMovieId = Guid.NewGuid();

            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.TestCharacterResponse.Name,
                Episodes = TestsConstants.TestCharacter.CharacterEpisodes.Select(s => s.EpisodeId).ToArray(),
                Friends = new[] {notExistingMovieId},
                Id = TestsConstants.NonExistingObjectGuid
            };
            // Act & Assert
            Assert.ThrowsAsync<InvalidOperationException>(
                async () => await _service.UpdateAsync(characterRequest, new CancellationToken()),
                $"Movie with id : {notExistingMovieId} does not exist in database");
        }


        [Test]
        public void Update_Character_When_Friend_With_Specified_Id_Not_Exists_It_Should_Throw_Exception()
        {
            //Arrange
            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.TestCharacterResponse.Name,
                Episodes = TestsConstants.TestCharacter.CharacterEpisodes.Select(s => s.EpisodeId).ToArray(),
                Friends = new[] {TestsConstants.NonExistingObjectGuid}
            };

            //Act & Assert
            Assert.ThrowsAsync<InvalidOperationException>(
                async () => await _service.UpdateAsync(characterRequest, new CancellationToken()),
                $"Character with id : {TestsConstants.NonExistingObjectGuid} does not exist in database");
        }

        [Test]
        public async Task Update_Should_Add_New_Friend_When_Request_Contains_New_Friend()
        {
            //Arrange
            var friendCharacterCopy = TestsConstants.FriendCharacter.Copy();
            var testCharacterCopy = TestsConstants.TestCharacter.Copy();

            var newFriendGuid = Guid.NewGuid();
            var newFriend = new Character {Name = "Randy Lahey", Id = newFriendGuid};

            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.TestCharacter.Id))
                .ReturnsAsync(testCharacterCopy);
            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(friendCharacterCopy);
            _characterRepositoryMock.Setup(m => m.GetById(newFriendGuid))
                .ReturnsAsync(newFriend);
            AddMoviesToRepositoryMock();

            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.FriendCharacter.Name,
                Episodes = TestsConstants.FriendCharacter.CharacterEpisodes.Select(s => s.EpisodeId).ToArray(),
                Friends = new[] {TestsConstants.TestCharacter.Id, newFriendGuid},
                Id = TestsConstants.FriendCharacter.Id
            };

            // Act
            await _service.UpdateAsync(characterRequest, new CancellationToken());

            //Assert
            var updatedObject = await _service.GetAsync(TestsConstants.FriendCharacter.Id, new CancellationToken());
            Assert.True(updatedObject.Friends.Contains(newFriend.Name) &&
                        updatedObject.Friends.Contains(TestsConstants.TestCharacter.Name));
        }

        [Test]
        public async Task Update_Should_Delete_All_Friends_When_Friends_Field_In_Request_Is_Null()
        {
            //Arrange
            var friendCharacterCopy = TestsConstants.FriendCharacter.Copy();
            var testCharacterCopy = TestsConstants.TestCharacter.Copy();

            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.TestCharacter.Id))
                .ReturnsAsync(testCharacterCopy);
            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(friendCharacterCopy);
            AddMoviesToRepositoryMock();

            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.FriendCharacter.Name,
                Episodes = TestsConstants.FriendCharacter.CharacterEpisodes.Select(s => s.EpisodeId).ToArray(),
                Friends = null,
                Id = TestsConstants.FriendCharacter.Id
            };

            // Act
            await _service.UpdateAsync(characterRequest, new CancellationToken());

            //Assert
            var updatedObject = await _service.GetAsync(TestsConstants.FriendCharacter.Id, new CancellationToken());
            Assert.True(updatedObject.Friends.Length == 0);
        }

        [Test]
        public async Task Update_Should_Delete_Episode_When_It_Is_Not_Present_In_Request()
        {
            //Arrange
            var friendCharacterCopy = TestsConstants.FriendCharacter.Copy();
            var testCharacterCopy = TestsConstants.TestCharacter.Copy();

            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.TestCharacter.Id))
                .ReturnsAsync(testCharacterCopy);
            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(friendCharacterCopy);
            AddMoviesToRepositoryMock();

            var episodeToStayInDb = TestsConstants.Episodes.First();

            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.FriendCharacter.Name,
                Episodes = new[] {episodeToStayInDb.Id},
                Friends = null,
                Id = TestsConstants.FriendCharacter.Id
            };

            // Act
            await _service.UpdateAsync(characterRequest, new CancellationToken());

            //Assert
            var updatedObject = await _service.GetAsync(TestsConstants.FriendCharacter.Id, new CancellationToken());
            Assert.True(updatedObject.Episodes.Length == 1);
            Assert.True(updatedObject.Episodes.Contains(episodeToStayInDb.Title));
        }

        [Test]
        public async Task Update_Should_Delete_Friend_Whent_It_Is_Not_Present_In_Request()
        {
            //Arrange
            var friendCharacterCopy = TestsConstants.FriendCharacter.Copy();
            var testCharacterCopy = TestsConstants.TestCharacter.Copy();

            var newFriendGuid = Guid.NewGuid();
            var newFriend = new Character {Name = "Randy Lahey", Id = newFriendGuid};

            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.TestCharacter.Id))
                .ReturnsAsync(testCharacterCopy);
            _characterRepositoryMock.Setup(m => m.GetById(TestsConstants.FriendCharacter.Id))
                .ReturnsAsync(friendCharacterCopy);
            _characterRepositoryMock.Setup(m => m.GetById(newFriendGuid))
                .ReturnsAsync(newFriend);
            AddMoviesToRepositoryMock();

            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.FriendCharacter.Name,
                Episodes = TestsConstants.FriendCharacter.CharacterEpisodes.Select(s => s.EpisodeId).ToArray(),
                Friends = new[] {newFriendGuid},
                Id = TestsConstants.FriendCharacter.Id
            };

            // Act
            await _service.UpdateAsync(characterRequest, new CancellationToken());

            //Assert
            var updatedObject = await _service.GetAsync(TestsConstants.FriendCharacter.Id, new CancellationToken());
            Assert.True(updatedObject.Friends.Contains(newFriend.Name) &&
                        !updatedObject.Friends.Contains(TestsConstants.TestCharacter.Name));
        }


        [Test]
        public void Update_Should_Throw_Exception_When_Object_Not_Exist_In_Database()
        {
            //Arrange
            var characterRequest = new CharacterRequest
            {
                Name = TestsConstants.FriendCharacter.Name,
                Episodes = TestsConstants.FriendCharacter.CharacterEpisodes.Select(s => s.EpisodeId).ToArray(),
                Friends = null,
                Id = TestsConstants.FriendCharacter.Id
            };

            // Act & Assert
            Assert.ThrowsAsync<InvalidOperationException>(
                async () => await _service.UpdateAsync(characterRequest, new CancellationToken()),
                $"Character with id : {TestsConstants.NonExistingObjectGuid} does not exist in database");
        }
    }
}