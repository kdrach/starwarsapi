﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using NUnit.Framework;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Episodes;
using StarWarsService.Application.Mapper;
using StarWarsService.Domain.Interfaces;
using StarWarsService.Domain.Models;
using StarWarsService.Tests.Shared.Helpers;
using StarWarsService.UnitTests.Helpers;

namespace StarWarsService.UnitTests.Application.Services
{
    [TestFixture]
    public class EpisodeServiceTest
    {
        //Arrange
        [SetUp]
        public void SetUp()
        {
            var config = new MapperConfiguration(cfg => { cfg.AddProfile(typeof(DefaultAutoMappingProfile)); });
            _mapper = config.CreateMapper();
            _episodeRepositoryMock = new Mock<IEpisodeRepository>();
            _service = new EpisodeService(_episodeRepositoryMock.Object, _mapper);
        }

        private Mock<IEpisodeRepository> _episodeRepositoryMock;
        private IMapper _mapper;
        private IEpisodeService _service;

        private Episode CreateEpisodeCopy(Episode episode)
        {
            return episode.Copy();
        }


        [Test]
        public async Task Create_Should_Not_Throw_Exception_And_Return_Newly_Created_Object()
        {
            //Arrange
            var request = _mapper.Map<Episode, EpisodeRequest>(TestsConstants.Episodes.First());

            //Act 
            var response = await _service.CreateAsync(request, new CancellationToken());

            //Assert
            Assert.AreEqual(TestsConstants.EpisodeResponses.First().Characters, response.Characters);
            Assert.AreEqual(TestsConstants.EpisodeResponses.First().Title, response.Title);
        }

        [Test]
        public async Task Delete_When_Object_Exists_In_Database_Should_Not_Throw_Exception_And_Return_Deleted_Object()
        {
            //Arrange
            var episodeCopy = CreateEpisodeCopy(TestsConstants.Episodes.First());
            _episodeRepositoryMock.Setup(m => m.GetById(episodeCopy.Id))
                .ReturnsAsync(episodeCopy);

            //Act 
            var response = await _service.DeleteAsync(episodeCopy.Id, new CancellationToken());

            //Assert
            Assert.IsTrue(
                new EpisodeResponseEqualityComparer().Equals(TestsConstants.EpisodeResponses.First(), response));
        }

        [Test]
        public void Delete_When_Object_Not_Exists_In_Database_Should_Throw_Exception()
        {
            //Arrange
            var notExistingEpisodeId = TestsConstants.NonExistingObjectGuid;

            //Act & Assert
            Assert.ThrowsAsync<InvalidOperationException>(
                async () => await _service.DeleteAsync(notExistingEpisodeId, new CancellationToken()),
                $"Episode with id {notExistingEpisodeId} not exists");
        }


        [Test]
        public async Task Get_Object_By_Id_When_Object_Exists_In_Database_It_Should_Return_Object()
        {
            //Arrange
            _episodeRepositoryMock.Setup(m => m.GetById(TestsConstants.Episodes.First().Id))
                .ReturnsAsync(TestsConstants.Episodes.First);

            //Act 
            var response = await _service.GetAsync(TestsConstants.Episodes.First().Id, new CancellationToken());

            //Assert
            Assert.True(
                new EpisodeResponseEqualityComparer().Equals(TestsConstants.EpisodeResponses.First(), response));
        }

        [Test]
        public async Task Get_Object_By_Id_When_Object_Not_Exists_In_Database_It_Should_Return_Null()
        {
            //Act 
            var response = await _service.GetAsync(Guid.NewGuid(), new CancellationToken());

            //Assert
            Assert.Null(response);
        }


        [Test]
        public async Task GetAllAsync_Should_Return_Empty_List_When_Database_Is_Empty()
        {
            //Arrange
            var emptyEntity = Enumerable.Empty<Episode>().ToList();
            _episodeRepositoryMock.Setup(m => m.GetAll(It.IsAny<PaginationOptions>()))
                .ReturnsAsync(new PagedList<Episode>(emptyEntity, emptyEntity.Count,
                    TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize));
            var emptyResponse = Enumerable.Empty<EpisodeResponse>().ToList();

            //Act
            var response = await _service.GetAllAsync(new CancellationToken(), TestsConstants.DefaultPaginationOptions);

            //Assert
            Assert.AreEqual(
                new PagedList<EpisodeResponse>(emptyResponse, emptyResponse.Count(),
                    TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize),
                response);
        }


        [Test]
        public async Task GetAllAsync_Should_Return_Object_List_When_Database_Is_Not_Empty()
        {
            //Arrange
            _episodeRepositoryMock.Setup(m => m.GetAll(TestsConstants.DefaultPaginationOptions)).ReturnsAsync(
                new PagedList<Episode>(TestsConstants.Episodes.ToList(),
                    TestsConstants.Episodes.Length, TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize));
            var expectedResponse = new PagedList<EpisodeResponse>(TestsConstants.EpisodeResponses.ToList(),
                TestsConstants.Episodes.Length, TestsConstants.DefaultPaginationOptions.PageNumber,
                TestsConstants.DefaultPaginationOptions.PageSize);

            //Act
            var response = await _service.GetAllAsync(new CancellationToken(), TestsConstants.DefaultPaginationOptions);

            //Assert
            Assert.AreEqual(expectedResponse.CurrentPage, response.CurrentPage);
            Assert.AreEqual(expectedResponse.PageSize, response.PageSize);
            Assert.AreEqual(expectedResponse.TotalCount, response.TotalCount);
            Assert.AreEqual(expectedResponse.TotalPages, response.TotalPages);
            Assert.AreEqual(expectedResponse.HasPrevious, response.HasPrevious);
            Assert.AreEqual(expectedResponse.HasNext, response.HasNext);
            Assert.True(expectedResponse.SequenceEqual(response, new EpisodeResponseEqualityComparer()));
        }


        [Test]
        public async Task Update_Should_Not_Throw_Exception_And_Return_Updated_Object()
        {
            //Arrange
            var episodeCopy = CreateEpisodeCopy(TestsConstants.Episodes.First());

            var request = _mapper.Map<Episode, EpisodeRequest>(episodeCopy);

            //Act 
            var response = await _service.UpdateAsync(request, new CancellationToken());

            //Assert
            Assert.IsTrue(
                new EpisodeResponseEqualityComparer().Equals(TestsConstants.EpisodeResponses.First(), response));
        }
    }
}