﻿using System.Collections.Generic;
using NUnit.Framework;

namespace StarWarsService.UnitTests.Helpers
{
    public class TestCaseDataSources
    {
        public static IEnumerable<TestCaseData> GetTestCaseDataForExistingAndNonExistingObject()
        {
            yield return new TestCaseData(TestsConstants.ExistingObjectGuid).SetName(
                "Test Case for Existing Character");
            yield return new TestCaseData(TestsConstants.NonExistingObjectGuid).SetName(
                "Test Case for NonExisting Character");
        }
    }
}