﻿using System.Collections.Generic;
using StarWarsService.Domain.Models;

namespace StarWarsService.UnitTests.Helpers
{
    public class CharacterEntityEqualityComparer : IEqualityComparer<Character>
    {
        public bool Equals(Character x, Character y)
        {
            if (ReferenceEquals(x, y)) return true;

            if (x is null || y is null) return false;

            return x.Name == y.Name && x.CharacterEpisodes == y.CharacterEpisodes &&
                   x.CharacterFriends == y.CharacterFriends &&
                   x.Id == y.Id;
        }

        public int GetHashCode(Character obj)
        {
            var hashCodeName = obj.Name == null ? 0 : obj.Name.GetHashCode();
            var hashCodeId = obj.Id.GetHashCode();
            var hashCodeFriends = obj.CharacterFriends == null ? 0 : obj.CharacterFriends.GetHashCode();
            var hashCodeEpisodes = obj.CharacterEpisodes == null ? 0 : obj.CharacterEpisodes.GetHashCode();

            return hashCodeName ^ hashCodeId ^ hashCodeFriends ^ hashCodeEpisodes;
        }
    }
}