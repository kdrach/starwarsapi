﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using StarWarsService.Api.Controllers;
using StarWarsService.Application.Dtos.Requests;
using StarWarsService.Application.Dtos.Responses;
using StarWarsService.Application.Mapper;
using StarWarsService.Domain.Models;

namespace StarWarsService.UnitTests.Helpers
{
    public static class TestsConstants
    {
        public static Guid ExistingObjectGuid { get; } = Guid.NewGuid();
        private static Guid ExistingFriendCharacterGuid { get; } = Guid.NewGuid();
        public static Guid NonExistingObjectGuid { get; } = Guid.NewGuid();

        private static MapperConfiguration Config { get; } = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(typeof(DefaultAutoMappingProfile));
        });

        private static IMapper Mapper { get; } = Config.CreateMapper();

        public static Episode[] Episodes { get; } =
        {
            new Episode {Id = Guid.NewGuid(), Title = "CLONES"},
            new Episode {Id = Guid.NewGuid(), Title = "EMPIRE"},
            new Episode {Title = "SITH", Id = Guid.NewGuid()}
        };

        public static EpisodeResponse[] EpisodeResponses { get; } = Mapper.Map<Episode[], EpisodeResponse[]>(Episodes);

        public static Character TestCharacter { get; } = new Character
        {
            Id = ExistingObjectGuid,
            CharacterEpisodes = Episodes.Select(s => new CharacterEpisode
            {
                Character = new Character {Name = "Luke Skywalker", Id = ExistingObjectGuid},
                CharacterId = ExistingObjectGuid, Episode = s, EpisodeId = s.Id
            }).ToList(),
            Name = "Luke Skywalker",
            FriendOf = new List<CharacterFriend>
            {
                new CharacterFriend
                {
                    Friend = new Character {Name = "Luke Skywalker", Id = ExistingObjectGuid},
                    FriendId = ExistingObjectGuid,
                    Character = new Character {Name = "Han Solo", Id = ExistingFriendCharacterGuid},
                    CharacterId = ExistingFriendCharacterGuid
                }
            },
            CharacterFriends = new List<CharacterFriend>
            {
                new CharacterFriend
                {
                    Character = new Character {Name = "Luke Skywalker", Id = ExistingObjectGuid},
                    CharacterId = ExistingObjectGuid,
                    Friend = new Character {Name = "Han Solo", Id = ExistingFriendCharacterGuid},
                    FriendId = ExistingFriendCharacterGuid
                }
            }
        };

        public static CharacterResponse TestCharacterResponse { get; } =
            Mapper.Map<Character, CharacterResponse>(TestCharacter);

        public static Character FriendCharacter { get; } = new Character
        {
            Id = ExistingFriendCharacterGuid,
            CharacterEpisodes = Episodes.Select(s => new CharacterEpisode
            {
                Character = new Character {Name = "Han Solo", Id = ExistingFriendCharacterGuid},
                CharacterId = ExistingFriendCharacterGuid, Episode = s, EpisodeId = s.Id
            }).ToList(),
            Name = "Han Solo",
            CharacterFriends = new List<CharacterFriend>
            {
                new CharacterFriend
                {
                    Character = new Character {Name = "Han Solo", Id = ExistingFriendCharacterGuid},
                    CharacterId = ExistingFriendCharacterGuid,
                    Friend = TestCharacter,
                    FriendId = TestCharacter.Id
                }
            },
            FriendOf = new List<CharacterFriend>
            {
                new CharacterFriend
                {
                    Friend = new Character {Name = "Han Solo", Id = ExistingFriendCharacterGuid},
                    FriendId = ExistingFriendCharacterGuid,
                    Character = TestCharacter,
                    CharacterId = TestCharacter.Id
                }
            }
        };

        public static CharacterResponse FriendCharacterResponse { get; } =
            Mapper.Map<Character, CharacterResponse>(FriendCharacter);

        public static List<CharacterResponse> CharactersResponses { get; } = new List<CharacterResponse>
            {TestCharacterResponse, FriendCharacterResponse};

        public static List<Character> CharacterEntitiesList { get; } = new List<Character>
            {TestCharacter, FriendCharacter};

        public static List<BaseEntity> BaseEntitiesList { get; } = new List<BaseEntity>
            {new BaseEntity {Id = Guid.NewGuid()}, new BaseEntity {Id = Guid.NewGuid()}};

        public static EpisodeRequest EpisodeRequest { get; } =
            new EpisodeRequest {Id = Guid.NewGuid(), Title = "Title"};

        public static CharacterRequest CharacterRequest { get; } = new CharacterRequest
        {
            Id = Guid.NewGuid(), Name = "Luke Skywalker", Episodes = Episodes.Select(s => s.Id).ToArray(),
            Friends = CharacterEntitiesList.Select(s => s.Id).ToArray()
        };

        public static GetAllAsyncParameters DefaultGetAllAsyncParameters { get; } =
            new GetAllAsyncParameters {Page = 1, PageSize = 50};

        public static PaginationOptions DefaultPaginationOptions { get; } =
            new PaginationOptions {PageNumber = 1, PageSize = 50};
    }
}