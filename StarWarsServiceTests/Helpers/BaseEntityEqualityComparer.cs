﻿using System.Collections.Generic;
using StarWarsService.Domain.Models;

namespace StarWarsService.UnitTests.Helpers
{
    public class BaseEntityEqualityComparer : IEqualityComparer<BaseEntity>
    {
        public bool Equals(BaseEntity x, BaseEntity y)
        {
            if (ReferenceEquals(x, y)) return true;

            if (x is null || y is null) return false;

            return x.Id == y.Id;
        }

        public int GetHashCode(BaseEntity obj)
        {
            var hashCodeId = obj.Id.GetHashCode();
            return hashCodeId;
        }
    }
}