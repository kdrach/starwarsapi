﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MockQueryable.Moq;
using NUnit.Framework;
using StarWarsService.Domain.Models;
using StarWarsService.Infrastructure.Pagination;
using StarWarsService.UnitTests.Helpers;

namespace StarWarsService.UnitTests.Infrastructure.Pagination
{
    [TestFixture]
    public class PagedListOperationTests
    {
        [TestCase]
        public async Task When_Source_Is_Empty_Paged_List_Should_Have_Expected_Values_Of_Properties()
        {
            //Arrange
            var queryableSource = new List<BaseEntity>().AsQueryable();
            var source = queryableSource.BuildMock();

            //Act
            var paggedList = await PagedListOperations<BaseEntity>.ToPagedList(source.Object,
                TestsConstants.DefaultPaginationOptions.PageNumber, TestsConstants.DefaultPaginationOptions.PageSize);

            //Assert
            Assert.AreEqual(paggedList.TotalCount, 0, "PaggedList from empty source should contains zero elements");
            Assert.AreEqual(paggedList.TotalPages, 0, "PaggedList from empty source should have zero pages");
            Assert.False(paggedList.HasNext, "PaggedList form empty source should not have next element");
            Assert.False(paggedList.HasPrevious
                , "PaggedList form empty source should not have previous element");
        }

        [TestCase(3)]
        [TestCase(50)]
        [TestCase(51)]
        [TestCase(1221)]
        public async Task When_Source_Is_Not_Empty_Paged_List_Should_Have_Expected_Values_Of_Properties(
            int numberOfElements)
        {
            //Arrange
            var listSource = Enumerable.Repeat(new BaseEntity(), numberOfElements);
            var pageSize = TestsConstants.DefaultPaginationOptions.PageSize;
            var expectedPageCount = (int) Math.Ceiling((double) numberOfElements / pageSize);
            var pageNumber = new Random().Next(0, expectedPageCount + 1);
            var shouldHaveNext = pageNumber < expectedPageCount;
            var shouldHavePrevious = pageNumber > 1;
            var queryableSource = listSource.AsQueryable();
            var source = queryableSource.BuildMock();

            //Act
            var paggedList = await PagedListOperations<BaseEntity>.ToPagedList(source.Object, pageNumber, pageSize);

            //Assert
            Assert.AreEqual(paggedList.TotalCount, numberOfElements,
                $"PaggedList  should contains {numberOfElements} elements");
            Assert.AreEqual(paggedList.TotalPages, expectedPageCount,
                $"PaggedList source should have {expectedPageCount} pages");
            Assert.AreEqual(paggedList.HasNext, shouldHaveNext, $"PaggedList HasNext should equals {shouldHaveNext}");
            Assert.AreEqual(paggedList.HasPrevious, shouldHavePrevious
                , $"PaggedList HasPrevious should equals {shouldHavePrevious}");
        }


        [TestCase(3, 1, 2)]
        [TestCase(50, 50, 1)]
        [TestCase(51, 50, 2)]
        [TestCase(1221, 100, 5)]
        [TestCase(1221, 2, 100)]
        public async Task When_Source_Of_PagedList_Is_Not_Empty_Items_Paged_List_Should_Contains_Source_Elements(
            int numberOfElements, int pageSize,
            int pageNumber)
        {
            //Arrange
            var listSource = Enumerable.Repeat(new BaseEntity {Id = Guid.NewGuid()}, numberOfElements);
            var expectedPageCount = (int) Math.Ceiling((double) numberOfElements / pageSize);
            var shouldHaveNext = pageNumber < expectedPageCount;
            var shouldHavePrevious = pageNumber > 1;
            var queryableSource = listSource.AsQueryable();
            var source = queryableSource.BuildMock();

            //Act
            var paggedList = await PagedListOperations<BaseEntity>.ToPagedList(source.Object,
                pageNumber, pageSize);

            //Assert
            Assert.True(
                source.Object.Skip((pageNumber - 1) * pageSize).Take(pageSize).SequenceEqual(paggedList.ToList()),
                "Elements should be equal to source");
            Assert.AreEqual(paggedList.TotalCount, numberOfElements,
                $"PaggedList  should contains {numberOfElements} elements");
            Assert.AreEqual(paggedList.TotalPages, expectedPageCount,
                $"PaggedList source should have {expectedPageCount} pages");
            Assert.AreEqual(paggedList.HasNext, shouldHaveNext, $"PaggedList HasNext should equals {shouldHaveNext}");
            Assert.AreEqual(paggedList.HasPrevious, shouldHavePrevious
                , $"PaggedList HasPrevious should equals {shouldHavePrevious}");
        }

        [Test]
        public async Task When_Source_Of_PagedList_Is_Empty_Items_Should_Also_Be_Empty()
        {
            //Arrange
            var queryableSource = new List<BaseEntity>().AsQueryable();
            var source = queryableSource.BuildMock();

            //Act
            var paggedList = await PagedListOperations<BaseEntity>.ToPagedList(source.Object,
                TestsConstants.DefaultPaginationOptions.PageNumber, TestsConstants.DefaultPaginationOptions.PageSize);

            //Assert
            Assert.True(source.Object.SequenceEqual(paggedList.ToList()), "PagedList should be empty");
        }
    }
}