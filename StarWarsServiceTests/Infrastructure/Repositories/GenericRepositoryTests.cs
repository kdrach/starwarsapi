﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using StarWarsService.Domain.Models;
using StarWarsService.Infrastructure;
using StarWarsService.Infrastructure.Repositories;
using StarWarsService.UnitTests.Helpers;

namespace StarWarsService.UnitTests.Infrastructure.Repositories
{
    [TestFixture]
    public class GenericRepositoryTests
    {
        [SetUp]
        public void SetUp()
        {
            CreateAndSetupRepository(Enumerable.Empty<BaseEntity>());
        }

        private StarWarsFakeContext _fakeContext;

        [TestCase]
        public async Task Get_When_Object_Exists_In_Context_It_Should_Be_Returned()
        {
            // Arrange
            var genericRepository = CreateAndSetupRepository(TestsConstants.BaseEntitiesList);

            // Act
            var objectFromRepo = await genericRepository.GetById(TestsConstants.BaseEntitiesList.First().Id);

            //Assert
            Assert.NotNull(objectFromRepo, "objectFromRepo should not be null");
            Assert.AreEqual(TestsConstants.BaseEntitiesList.First(), objectFromRepo);
        }

        [TestCase]
        public async Task Get_When_Object_Not_Exists_In_Context_Null_Should_Be_Returned()
        {
            // Arrange
            var genericRepository = CreateAndSetupRepository(TestsConstants.BaseEntitiesList);

            // Act
            var objectFromRepo = await genericRepository.GetById(TestsConstants.NonExistingObjectGuid);

            //Assert
            Assert.IsNull(objectFromRepo, "objectFromRepo should be null");
        }


        [TestCase]
        public async Task GetAll_When_DB_Set_Is_Not_Empty_List_Should_Be_Returned()
        {
            // Arrange
            var genericRepository = CreateAndSetupRepository(TestsConstants.BaseEntitiesList);

            // Act
            var objectFromRepo = await genericRepository.GetAll(TestsConstants.DefaultPaginationOptions);

            //Assert
            Assert.IsNotNull(objectFromRepo, "objectFromRepo should not be null");
            Assert.AreEqual(
                new PagedList<BaseEntity>(TestsConstants.BaseEntitiesList, TestsConstants.BaseEntitiesList.Count,
                    TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize),
                objectFromRepo);
        }

        [TestCase]
        public async Task GetAll_When_DB_Set_Is_Empty_EmptyList_Should_Be_Returned()
        {
            // Arrange
            var genericRepository = CreateAndSetupRepository(Enumerable.Empty<Character>().AsQueryable());

            // Act
            var objectFromRepo = await genericRepository.GetAll(TestsConstants.DefaultPaginationOptions);

            //Assert
            Assert.IsNotNull(objectFromRepo, "objectFromRepo should not be null");
            var emptyList = Enumerable.Empty<BaseEntity>().ToList();
            Assert.AreEqual(
                new PagedList<BaseEntity>(emptyList, emptyList.Count,
                    TestsConstants.DefaultPaginationOptions.PageNumber,
                    TestsConstants.DefaultPaginationOptions.PageSize), objectFromRepo);
        }

        [TestCase]
        public async Task Add_When_Character_Not_Exists_In_DB_Should_Be_Added()
        {
            //Arrange
            var genericRepository = CreateAndSetupRepository(Enumerable.Empty<BaseEntity>());
            var objectToAdd = new BaseEntity
            {
                Id = TestsConstants.ExistingObjectGuid
            };

            //Act
            await genericRepository.Add(objectToAdd);
            var newlyAddedObject = await genericRepository.GetById(TestsConstants.ExistingObjectGuid);

            //Assert
            Assert.AreEqual(objectToAdd, newlyAddedObject);
        }


        [TestCase]
        public async Task Remove_When_Object_Exist_In_Db()
        {
            //Arrange
            var genericRepository = CreateAndSetupRepository(TestsConstants.BaseEntitiesList);
            CreateAndSetupRepository(TestsConstants.BaseEntitiesList);

            var objectFromRepo = await genericRepository.GetById(TestsConstants.BaseEntitiesList.First().Id);

            //Act
            await genericRepository.Remove(objectFromRepo);

            //Assert
            var removedObject = await genericRepository.GetById(TestsConstants.BaseEntitiesList.First().Id);
            Assert.AreEqual(null, removedObject);
        }

        private GenericRepository<BaseEntity> CreateAndSetupRepository(IEnumerable<BaseEntity> baseEntities)
        {
            var options = new DbContextOptionsBuilder<StarWarsContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            _fakeContext = new StarWarsFakeContext(options);
            _fakeContext.AddRange(baseEntities);
            _fakeContext.SaveChanges();

            return new GenericRepository<BaseEntity>(_fakeContext);
        }
    }
}