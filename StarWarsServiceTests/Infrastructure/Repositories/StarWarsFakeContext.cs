﻿using Microsoft.EntityFrameworkCore;
using StarWarsService.Domain.Models;
using StarWarsService.Infrastructure;

namespace StarWarsService.UnitTests.Infrastructure.Repositories
{
    public class StarWarsFakeContext : StarWarsContext
    {
        public StarWarsFakeContext(DbContextOptions<StarWarsContext> options) : base(options)
        {
        }

        public virtual DbSet<BaseEntity> BaseEntities { get; set; }
    }
}